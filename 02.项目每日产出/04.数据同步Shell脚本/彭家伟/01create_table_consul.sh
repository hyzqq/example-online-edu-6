#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
-- 建表

drop table if exists edu_ods.ods_web_chat_ems_2019_07;
create table edu_ods.ods_web_chat_ems_2019_07
(
    id                           int comment '主键',
    create_date_time             timestamp comment '数据创建时间',
    session_id                   string comment '会话系统id,计算会话数',
    sid                          string comment '访客id，计算访客数',
    create_time                  string comment '会话创建时间',
    seo_source                   string comment '搜索来源',
    seo_keywords                 string comment '关键字',
    ip                           string comment 'IP地址，计算IP数',
    area                         string comment '地域',
    country                      string comment '所在国家',
    province                     string comment '省',
    city                         string comment '城市',
    origin_channel               string comment '来源渠道(广告)',
    `user`                       string comment '所属坐席',
    manual_time                  string comment '人工开始时间',
    begin_time                   string comment '坐席领取时间 ',
    end_time                     string comment '会话结束时间',
    last_customer_msg_time_stamp string comment '客户最后一条消息的时间',
    last_agent_msg_time_stamp    string comment '坐席最后一下回复的时间',
    reply_msg_count              int comment '客服回复消息数',
    msg_count                    int comment '客户发送消息数',
    browser_name                 string comment '浏览器名称',
    os_info                      string comment '系统名称'
) comment '用户与咨询主表'
partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_ods.ods_web_chat_text_ems_2019_07;
create table edu_ods.ods_web_chat_text_ems_2019_07
(
    id                   int comment '主键',
    referrer             string comment '上级来源页面',
    from_url             string comment '会话来源页面',
    landing_page_url     string comment '访客着陆页面',
    url_title            string comment '咨询页面title',
    platform_description string comment '客户平台信息',
    other_params         string comment '扩展字段中数据',
    history              string comment '历史访问记录'
)comment '用户与咨询副表'
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- dwd层建表语句

drop table if exists edu_dwd.dwd_fact_web_chat_ems;
create table if not exists edu_dwd.dwd_fact_web_chat_ems(
    id							   int comment '主键',
    create_date_time        	   timestamp comment '数据创建时间',
    session_id              	   string comment '会话系统id,计算会话数',
    sid                     	   string comment '访客id，计算访客数',
    create_time             	   timestamp comment '会话创建时间',
    seo_source              	   string comment '搜索来源',
    ip                      	   string comment 'IP地址，计算IP数',
    area                    	   string comment '地域',
    country                 	   string comment '所在国家',
    province                       string comment '省',
    city                           string comment '城市',
    origin_channel                 string comment '来源渠道(广告)',
    msg_count                      int comment '客户发送消息数',
    browser_name                   string comment '浏览器名称',
    os_info                        string comment '系统名称',
    from_url             		   string comment '会话来源页面'
)comment'用户与咨询事实表'
partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_dwd.dwd_dim_area;
create table if not exists edu_dwd.dwd_dim_area(
    id			int	comment '主键' ,
    area        string comment '地域',
    country     string comment '所在国家',
    province    string comment '省',
    city        string comment '城市'
)comment'地区维度表'
row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_dwd.dwd_dim_date;
create table if not exists edu_dwd.dwd_dim_date(
    id						int	comment '主键',
    create_date_time        timestamp comment '数据创建时间',
    create_time             timestamp comment '会话创建时间'
)comment'时间维度表'
row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- dwb层建表语句

drop table if exists edu_dwb.dwb_web_chat_ems_detail;
create table if not exists edu_dwb.dwb_web_chat_ems_detail(
    -- 原来的字段
    id							   int	comment '主键',
    create_date_time        	   timestamp comment '数据创建时间',
    session_id              	   string comment '会话系统id,计算会话数',
    sid                     	   string comment '访客id，计算访客数',
    create_time             	   timestamp comment '会话创建时间',
    seo_source              	   string comment '搜索来源',
    ip                      	   string comment 'IP地址，计算IP数',
    area                    	   string comment '地域',
    country                 	   string comment '所在国家',
    province                       string comment '省',
    city                           string comment '城市',
    origin_channel                 string comment '来源渠道(广告)',
    msg_count                      int comment '客户发送消息数',
    browser_name                   string comment '浏览器名称',
    os_info                        string comment '系统名称',
    -- 从副表关联得到的字段
    from_url             		   string comment'会话来源页面',
    -- 拉链表字段
    endTime						   string comment '拉链表状态终止时间'
)partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- dws层建表语句

drop table if exists edu_dws.dws_web_chat_ems_daycount;
create table if not exists edu_dws.dws_web_chat_ems_daycount
(
    -- 维度
    date_hour      string comment '小时维度',
    country        string comment '所在国家',
    province       string comment '省',
    city           string comment '城市',
    origin_channel string comment '来源渠道(广告)',
    seo_source     string comment '搜索来源',
    from_url       string comment '会话来源页面',
    group_type     string comment '分组标记',
    --   =======日统计=======
    user_cnt       bigint comment '总访问用户量',
    ip_cnt         bigint comment '总访问ip个数',
    session_cnt    bigint comment '总访问session个数',
    consul_cnt     bigint comment '咨询人数'
) partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- dm层建表语句

drop table if exists edu_dm.dm_web_chat_ems;
create table if not exists edu_dm.dm_web_chat_ems
(
    date_time      string comment '统计日期,用来标记统计日期',
    --时间粒度标记
    time_type      string comment '统计时间维度：year、quarter、month、date(就是天day)、hour',
    --时间粒度字段
    year_code      string comment '年,如2014',
    quarter_code   string comment '季度',
    year_month     string comment '年月,如201401',
    year_month_day string comment '天，如20240503',
    date_hour      string comment '小时',

    -- 维度
    group_type     string comment '分组类型：',
    country        string comment '所在国家',
    province       string comment '省',
    city           string comment '城市',
    origin_channel string comment '来源渠道(广告)',
    seo_source     string comment '搜索来源',
    from_url       string comment '会话来源页面',
    --   =======统计=======
    user_nums       bigint comment '总访问用户量',
    ip_nums         bigint comment '总访问ip个数',
    session_nums    bigint comment '总访问session个数',
    consul_nums     bigint comment '咨询人数'
)
    partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- rpt层建表语句

-- 总访问客户量建表语句
drop table if exists edu_rpt.user_cnt;
create table if not exists edu_rpt.user_cnt(
    date_type string comment '时间类型',
    time_type string comment '分组标记',
    user_nums bigint
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 地区独立访客热力图建表语句
drop table if exists edu_rpt.distri_cnt;
create table if not exists edu_rpt.distri_cnt(
    distr_type string comment '地区类型',
    group_type string comment '分组标记',
    user_nums bigint
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 访客咨询率趋势建表语句
drop table if exists edu_rpt.consul_rate_cnt;
create table if not exists edu_rpt.consul_rate_cnt(
    date_type string comment '时间',
    time_type string comment '时间粒度',
    group_type string comment '地区粒度',
    distr_type string comment '地区',
    consul_rate decimal(32,2) comment '咨询率'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 时间段访问客户量趋势建表语句

drop table if exists edu_rpt.hour_cnt;
create table if not exists edu_rpt.hour_cnt(
    date_hour string comment '天-小时',
    user_nums bigint
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');
-- 来源渠道访问量占比建表语句

drop table if exists edu_rpt.origin_cnt;
create table if not exists edu_rpt.origin_cnt(
    date_type string comment '时间类型',
    origin_channel string comment '来源渠道',
    rate decimal(22,2) comment '比率'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 搜索来源访问量占比建表语句
drop table if exists edu_rpt.source_cnt;
create table if not exists edu_rpt.source_cnt(
    date_type string comment '时间类型',
    seo_source string comment '搜索来源',
    rate decimal(22,2) comment '比率'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 活跃页面排行榜建表语句
drop table if exists edu_rpt.from_url_cnt;
create table if not exists edu_rpt.from_url_cnt(
    time_type string comment '时间标记',
    date_type string comment '时间类型',
    from_url string comment '来源页面',
    nums    bigint comment '用户数量',
    rn decimal(22,2) comment '排名'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- olap建表语句

-- 总访问客户量建表语句
drop table if exists edu_olap.olap_user_cnt;
create table if not exists edu_olap.olap_user_cnt(
    date_type varchar comment '时间类型',
    time_type varchar comment '分组标记',
    user_nums bigint
);

-- 地区独立访客热力图建表语句
drop table if exists edu_olap.olap_distri_cnt;
create table if not exists edu_olap.olap_distri_cnt(
    distr_type varchar comment '地区类型',
    group_type varchar comment '分组标记',
    user_nums bigint
);

-- 访客咨询率趋势建表语句
drop table if exists edu_olap.olap_consul_rate_cnt;
create table if not exists edu_olap.olap_consul_rate_cnt(
    date_type varchar comment '时间',
    time_type varchar comment '时间粒度',
    group_type varchar comment '地区粒度',
    distr_type varchar comment '地区',
    consul_rate decimal(32,2) comment '咨询率'
);

-- 时间段访问客户量趋势建表语句

drop table if exists edu_olap.olap_hour_cnt;
create table if not exists edu_olap.olap_hour_cnt(
    date_hour varchar comment '天-小时',
    user_nums bigint
);
-- 来源渠道访问量占比建表语句

drop table if exists edu_olap.olap_origin_cnt;
create table if not exists edu_olap.olap_origin_cnt(
    date_type varchar comment '时间类型',
    origin_channel varchar comment '来源渠道',
    rate decimal(22,2) comment '比率'
);

-- 搜索来源访问量占比建表语句
drop table if exists edu_olap.olap_source_cnt;
create table if not exists edu_olap.olap_source_cnt(
    date_type varchar comment '时间类型',
    seo_source varchar comment '搜索来源',
    rate decimal(22,2) comment '比率'
);

-- 活跃页面排行榜建表语句
drop table if exists edu_olap.olap_from_url_cnt;
create table if not exists edu_olap.olap_from_url_cnt(
    time_type varchar comment '时间标记',
    date_type varchar comment '时间类型',
    from_url varchar comment '来源页面',
    nums bigint comment '用户人数',
    rn decimal(22,2) comment '排名'
);
"