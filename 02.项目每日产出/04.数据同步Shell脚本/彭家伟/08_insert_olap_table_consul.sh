#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
--分区,非严格模式
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;

insert into mysql.edu_olap.olap_consul_rate_cnt
select * from hive.edu_rpt.consul_rate_cnt;

insert into mysql.edu_olap.olap_distri_cnt
select * from hive.edu_rpt.distri_cnt;

insert into mysql.edu_olap.olap_hour_cnt
select * from hive.edu_rpt.hour_cnt;

insert into mysql.edu_olap.olap_origin_cnt
select * from hive.edu_rpt.origin_cnt;

insert into mysql.edu_olap.olap_source_cnt
select * from hive.edu_rpt.source_cnt;

insert into mysql.edu_olap.olap_user_cnt
select * from hive.edu_rpt.user_cnt;

insert into mysql.edu_olap.olap_from_url_cnt
select * from hive.edu_rpt.from_url_cnt;
"