#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
--分区,非严格模式
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;

insert overwrite table edu_dwd.dwd_fact_web_chat_ems partition(dt)
select a.id,
       a.create_date_time,
       a.session_id,
       a.sid,
       a.create_time,
       a.seo_source,
       a.ip,
       a.area,
       a.country,
       a.province,
       a.city,
       a.origin_channel,
       a.msg_count,
       a.browser_name,
       a.os_info,
       b.from_url,
       substr(a.create_time,1,10) as dt
from edu_ods.ods_web_chat_ems_2019_07 a
         left join edu_ods.ods_web_chat_text_ems_2019_07 b on a.id = b.id;

insert overwrite table edu_dwd.dwd_dim_area
select
    id,
    area,
    country,
    province,
    city
from edu_ods.ods_web_chat_ems_2019_07;

insert overwrite table edu_dwd.dwd_dim_date
select
    id,
    substring(create_date_time,1,19) as create_date_time,
    substring(create_time,1,19) as create_time
from edu_ods.ods_web_chat_ems_2019_07;
"