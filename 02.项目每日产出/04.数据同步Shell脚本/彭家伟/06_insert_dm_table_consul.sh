#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
--分区,非严格模式
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;

insert into hive.edu_dm.dm_web_chat_ems
with t as (
    select
    *,
    substring(dt,1,4) as year_code,
    concat(
       cast(extract(year from cast(dt as date)) as varchar ),
       '-q',
       cast(ceil(extract(month from cast(dt as date))*1.00 / 3) as varchar )
       ) as quarter_code,
    substring(dt,1,7) as year_month,
    substring(dt,1,10) as year_month_day
from hive.edu_dws.dws_web_chat_ems_daycount
)
select
    '2024-05-04' as date_time,
-- 时间维度
    case when grouping(year_code,quarter_code,year_month,year_month_day,date_hour) = 0
            then 'hour'
         when grouping(year_code,quarter_code,year_month,year_month_day) = 0
            then 'day'
         when grouping(year_code,quarter_code,year_month) = 0
            then 'month'
         when grouping(year_code,quarter_code) = 0
            then 'quarter'
         when grouping(year_code) = 0
            then 'year'
         else 'other' end as time_type,
    year_code,
    quarter_code,
    year_month,
    year_month_day,
    date_hour,
-- 指标维度
    case when grouping(country,province,city) = 0
            then 'city'
         when grouping(country,province) = 0
            then 'province'
         when grouping(country) = 0
            then 'country'
         when grouping(origin_channel) = 0
            then 'origin_channel'
         when grouping(seo_source) = 0
            then 'seo_source'
         when grouping(from_url) = 0
            then 'from_url'
         else 'all' end as group_type,
    country,
    province,
    city,
    origin_channel,
    seo_source,
    from_url,
-- 指标的聚合
    sum(user_cnt) as user_nums,
    sum(ip_cnt) as ip_nums,
    sum(session_cnt) as session_nums,
    sum(consul_cnt) as consul_nums,
    t.year_month_day as dt
from t
group by
grouping sets(
-- 年
    (year_code),
    (year_code,country),
    (year_code,country,province),
    (year_code,country,province,city),
    (year_code,origin_channel),
    (year_code,seo_source),
    (year_code,from_url),
-- 季度
    (year_code,quarter_code),
    (year_code,quarter_code,country),
    (year_code,quarter_code,country,province),
    (year_code,quarter_code,country,province,city),
    (year_code,quarter_code,origin_channel),
    (year_code,quarter_code,seo_source),
    (year_code,quarter_code,from_url),
-- 月
    (year_code,quarter_code,year_month),
    (year_code,quarter_code,year_month,country),
    (year_code,quarter_code,year_month,country,province),
    (year_code,quarter_code,year_month,country,province,city),
    (year_code,quarter_code,year_month,origin_channel),
    (year_code,quarter_code,year_month,seo_source),
    (year_code,quarter_code,year_month,from_url),
-- 日
    (year_code,quarter_code,year_month,year_month_day),
    (year_code,quarter_code,year_month,year_month_day,country),
    (year_code,quarter_code,year_month,year_month_day,country,province),
    (year_code,quarter_code,year_month,year_month_day,country,province,city),
    (year_code,quarter_code,year_month,year_month_day,origin_channel),
    (year_code,quarter_code,year_month,year_month_day,seo_source),
    (year_code,quarter_code,year_month,year_month_day,from_url),
-- 小时
    (year_code,quarter_code,year_month,year_month_day,date_hour),
    (year_code,quarter_code,year_month,year_month_day,date_hour,country),
    (year_code,quarter_code,year_month,year_month_day,date_hour,country,province),
    (year_code,quarter_code,year_month,year_month_day,date_hour,country,province,city),
    (year_code,quarter_code,year_month,year_month_day,date_hour,origin_channel),
    (year_code,quarter_code,year_month,year_month_day,date_hour,seo_source),
    (year_code,quarter_code,year_month,year_month_day,date_hour,from_url)
    );
"