#! /bin/bash
#SQOOP_HOME=/opt/cloudera/parcels/CDH-6.2.1-1.cdh6.2.1.p0.1425774/bin/sqoop
export LANG=zh_CN.UTF-8
SQOOP_HOME=/usr/bin/sqoop
if [[ $1 == "" ]];then
   DATE=`date -d '1 days ago' "+%Y-%m-%d"`
else
   DATE=$1
fi

echo '========================================'
echo '==============开始全量导入==============='
echo '========================================'

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *,'${date}' as dt from web_chat_ems_2019_07 where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table ods_web_chat_ems_2019_07 \
-m 1
wait

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select * from web_chat_text_ems_2019_07 where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table ods_web_chat_text_ems_2019_07 \
-m 1

echo '========================================'
echo '=================导入完成==============='
echo '========================================'