#! /bin/bash
#SQOOP_HOME=/opt/cloudera/parcels/CDH-6.2.1-1.cdh6.2.1.p0.1425774/bin/sqoop
export LANG=zh_CN.UTF-8
SQOOP_HOME=/usr/bin/sqoop
if [[ $1 == "" ]];then
   DATE=`date -d '1 days ago' "+%Y-%m-%d"`
else
   DATE=$1
fi

echo '========================================'
echo '==============开始全量导入==============='
echo '========================================'

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *,'${DATE}' as dt from tbh_student_signin_record where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table tbh_student_signin_record \
-m 1
wait

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *,'${DATE}' as dt from student_leave_apply where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table student_leave_apply \
-m 1
wait

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select * from tbh_class_time_table where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table tbh_class_time_table \
-m 1
wait

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select * from course_table_upload_detail where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table course_table_upload_detail \
-m 1
wait

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select * from class_studying_student_count where 1=1 and  \$CONDITIONS" \
--hcatalog-database edu_ods \
--hcatalog-table class_studying_student_count \
-m 1
wait

echo '========================================'
echo '=================success==============='
echo '========================================'
