#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--考勤主题宽表

insert into table edu_dwb.tbh_student_signin_record_detail partition (dt)
select t1.id,
       normal_class_flag,
       time_table_id,
       t1.class_id,
       t1.student_id,
       signin_time,
       signin_date,
       share_state,
       audit_state,
       begin_time,
       begin_time_type,
       end_time,
       end_time_type,
       days,
       valid_state,
       t2.create_time,
       morning_template_id,
       morning_begin_time,
       morning_end_time,
       afternoon_template_id,
       afternoon_begin_time,
       afternoon_end_time,
       evening_template_id,
       evening_begin_time,
       evening_end_time,
       base_id,
       class_date,
       school_id,
       studying_student_count,
       studying_date,
       t1.dt
from edu_dwd.fact_tbh_student_signin_record t1
left join edu_dwd.fact_student_leave_apply t2 on t1.student_id = t2.student_id
left join edu_dwd.dim_tbh_class_time_table t3 on t1.time_table_id = t3.id
left join edu_dwd.dim_course_table_upload_detail t4 on t1.class_id = t4.class_id
left join edu_dwd.dim_class_studying_student_count t5 on t1.class_id = t5.class_id and t1.signin_date=t5.studying_date;
"