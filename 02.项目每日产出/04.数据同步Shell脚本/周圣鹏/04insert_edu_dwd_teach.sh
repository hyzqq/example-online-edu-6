#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;

--学生打卡表
insert overwrite table edu_dwd.fact_tbh_student_signin_record partition (dt)
select id,
       case normal_class_flag
           when 1 then '正课'
           when 2 then '自习'
           else 'other' end          normal_class_flag,
       time_table_id,
       class_id,
       student_id,
       signin_time,
       signin_date,
       share_state,
       '9999-99-99'                  end_date,
        dt
from edu_ods.tbh_student_signin_record;

--学生请假表
insert overwrite table edu_dwd.fact_student_leave_apply partition (dt)
select id,
       class_id,
       student_id,
       case audit_state
           when 0 then '待审核'
           when 1 then '通过'
           when 2 then '不通过'
           else 'other' end       as audit_state,
       begin_time,
       case begin_time_type
           when 1 then '上午'
           when 2 then '下午'
           else 'other' end       as begin_time_type,
       end_time,
       case end_time_type
           when 1 then '上午'
           when 2 then '下午'
           else 'other' end       as end_time_type,
       days,
       valid_state,
       create_time,
       '9999-99-99'               as end_date,
       dt
from edu_ods.student_leave_apply;

--班级作息表
insert overwrite table edu_dwd.dim_tbh_class_time_table 
select id,
       class_id,
       morning_template_id,
       morning_begin_time,
       morning_end_time,
       afternoon_template_id,
       afternoon_begin_time,
       afternoon_end_time,
       evening_template_id,
       evening_begin_time,
       evening_end_time,
       create_time
from edu_ods.tbh_class_time_table;

--班级排课
insert overwrite table edu_dwd.dim_course_table_upload_detail 
select id,
       base_id,
       class_id,
       class_date,
       create_time
from edu_ods.course_table_upload_detail;

-- 在读学员信息
insert overwrite table edu_dwd.dim_class_studying_student_count 
select
       id,
       school_id,
       subject_id,
       class_id,
       studying_student_count,
       studying_date
from edu_ods.class_studying_student_count;
"