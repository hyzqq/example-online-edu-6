#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
--考勤分析宽表表
create table edu_dwb.tbh_student_signin_record_detail
(
    id                    int comment '主键id',
    normal_class_flag     string comment '是否正课 1 正课 2 自习',
    time_table_id         int comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id              int comment '班级id',
    student_id            int comment '学员id',
    signin_time           string comment '签到时间',
    signin_date           string comment '签到日期',
    share_state           string comment '共享屏幕状态 0 否 1是  在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0 ',
-- 从请假表关联得到的字段
    audit_state           int comment '审核状态 0 待审核 1 通过 2 不通过',
    begin_time            string comment '请假开始时间',
    begin_time_type       string comment '1：上午 2：下午',
    end_time              string comment '请假结束时间',
    end_time_type         string comment '1：上午 2：下午',
    days                  decimal(38, 2) comment '请假/已休天数',
    valid_state           int comment '是否有效（0：无效 1：有效）',
    create_time           string comment '创建时间',
    -- 从班级作息表关联得到的字段
    morning_template_id   int comment '上午出勤模板id',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id int comment '下午出勤模板id',
    afternoon_begin_time  string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time      string comment '晚上结束时间',
-- 从班级排课表关联得到字段
    base_id               int comment '课程主表id',
    class_date            string comment '上课日期',
-- 在读学员人数表得到字段
    school_id              int     comment '校区id',
    studying_student_count int     comment '在读班级人数',
    studying_date          string  comment '在读日期'
)comment '考勤打卡信息表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
"