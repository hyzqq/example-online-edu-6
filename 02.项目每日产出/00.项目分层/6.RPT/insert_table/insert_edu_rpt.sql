insert into  hive.edu_rpt.student_attendance
select
class_id,
studying_student_count,
       class_date ,
coalesce (moring_attendance_cnt,0),
coalesce (moring_attendance_pre,0),
coalesce (afternoon_attendance_cnt,0),
coalesce (afternoon_attendance_pre,0),
coalesce (evening_attendance_cnt,0),
coalesce (evening_attendance_pre,0)
from hive.edu_dws.student_class_situation;

insert into  hive.edu_rpt.student_arrive_late
select
class_id,
studying_student_count,
             class_date ,
coalesce (moring_arrive_late_cnt,0),
coalesce (moring_arrive_late_pre,0),
coalesce (afternoon_arrive_late_cnt,0),
coalesce (afternoon_arrive_late_pre,0),
coalesce (evening_arrive_late_cnt,0),
coalesce (evening_arrive_late_pre,0)
from hive.edu_dws.student_class_situation;

insert into  hive.edu_rpt.student_take_off
select
class_id,
studying_student_count,
             class_date ,
coalesce (moring_take_off_cnt,0),
coalesce (moring_take_off_pre,0),
coalesce (afternoon_take_off_cnt,0),
coalesce (afternoon_take_off_pre,0),
coalesce (evening_take_off_cnt,0),
coalesce (evening_take_off_pre,0)
from hive.edu_dws.student_class_situation;


insert into  hive.edu_rpt.student_volume_up_truancy
select
class_id,
studying_student_count,
             class_date ,
coalesce (moring_volume_up_truancy_cnt,0),
coalesce (moring_volume_up_truancy_pre,0),
coalesce (afternoon_volume_up_truancy_cnt,0),
coalesce (afternoon_volume_up_truancy_pre,0),
coalesce (evening_volume_up_truancy_cnt,0),
coalesce (evening_truancy_pre,0)
from hive.edu_dws.student_class_situation;



CREATE TABLE mysql.edu_olap.student_attendance
AS
SELECT * FROM  hive.edu_rpt.student_attendance

CREATE TABLE mysql.edu_olap.student_arrive_late
AS
SELECT * FROM  hive.edu_rpt.student_arrive_late

CREATE TABLE mysql.edu_olap.student_take_off
AS
SELECT * FROM  hive.edu_rpt.student_take_off

CREATE TABLE mysql.edu_olap.student_volume_up_truancy
AS
SELECT * FROM  hive.edu_rpt.student_volume_up_truancy