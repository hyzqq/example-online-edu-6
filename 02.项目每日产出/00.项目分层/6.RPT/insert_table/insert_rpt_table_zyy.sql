--todo 每天每月每年各个校区的报名人数
insert into hive.edu_rpt.scpay
with a as
(select
substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
"day" as time ,sc_name, cnt_sc
from hive.edu_dws.dws_customer_detail_cnt
where cnt_sc is not null )
select
    distinct *,
sum(cnt_sc) over(partition by "month"  ) as c_m,
sum(cnt_sc) over(partition by "year" ) as c_y
from a ;

--todo 每天每月每年线上线下各个校区的报名人数、
insert into hive.edu_rpt.scopay
with a as
(select
substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
"day" as time ,sc_name, cnt_o_sc
from hive.edu_dws.dws_customer_detail_cnt
where cnt_o_sc is not null )
select
    distinct *,
sum(cnt_o_sc) over(partition by "month"  ) as c_m,
sum(cnt_o_sc) over(partition by "year" ) as c_y
from a ;

--todo 每天每月每年线上线下各个学科的报名人数、
insert into hive.edu_rpt.suopay
with a as
(select
substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
"day" as time ,name, cnt_o_su
from hive.edu_dws.dws_customer_detail_cnt
where cnt_o_su is not null )
select
    distinct *,
sum(cnt_o_su) over(partition by "month"  ) as c_m,
sum(cnt_o_su) over(partition by "year" ) as c_y
from a ;

--todo 每天每月每年线上线下各个校区学科的报名人数、
insert into hive.edu_rpt.scsuopay
with a as
(select
substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
"day" as time ,name, sc_name,cnt_o_su,cnt_o_sc
from hive.edu_dws.dws_customer_detail_cnt
where cnt_o_su is not null and cnt_o_sc is not null  and name is not null  )
select
    distinct *,
sum(cnt_o_su) over(partition by "month"  ) as su_m,
sum(cnt_o_su) over(partition by "year" ) as su_y,
sum(cnt_o_sc) over(partition by "month" ) as sc_m,
sum(cnt_o_sc) over(partition by "year" ) as sc_y
from a ;

--todo 每天每月每年线上线下各个来源渠道的报名人数、
insert into hive.edu_rpt.quopay
with a as
(select
substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
"day" as time ,origin_channel, cnt_qu
from hive.edu_dws.dws_customer_detail_cnt
where cnt_qu is not null )
select
    distinct *,
sum(cnt_qu) over(partition by "month") as c_m,
sum(cnt_qu) over(partition by "year") as c_y
from a ;

--todo 每天每月每年线上线下各个咨询中心的报名人数、
insert into hive.edu_rpt.ziopay
with a as
(select
substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
"day" as time ,depart_name, cnt_zi
from hive.edu_dws.dws_customer_detail_cnt
where cnt_zi is not null )
select
    distinct *,
sum(cnt_zi) over(partition by "month" ) as c_m,
sum(cnt_zi) over(partition by "year" ) as c_y
from a ;

--todo 每天每月每年线上线下意向报名转换率
insert into hive.edu_rpt.rage_yi
with a as
(select
       "day" as time,
       substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
       origin_type_state,
       cast(cnt_yi as double) as cnt1_yi ,
       cast(cnt_o_sc as double) as cnt1_o_sc
from hive.edu_dws.dws_customer_detail_cnt
where origin_type_state is not null )
,b as (select
       time,
       month,year,
       origin_type_state,
  cnt1_o_sc,  cnt1_yi,
(cnt1_o_sc/cnt1_yi) as rage_day,
sum(cnt1_o_sc) over(partition by "month" ) as c_yi_m,
sum(cnt1_yi) over(partition by "month" ) as c_osc_m,
sum(cnt1_o_sc) over(partition by "year" ) as c_yi_y,
sum(cnt1_yi) over(partition by "year" ) as c_osc_y
from a
where cnt1_yi is not null and cnt1_o_sc is not null)
,c as (select * ,(c_osc_m/c_yi_m) as rage_month,
       (c_osc_y/c_yi_y) as rage_year from b where rage_day >=0)
       select * from c
       where rage_month >0 and rage_year > 0 and rage_day > 0;

--todo 每天每月每年线上有效线索报名率
insert into hive.edu_rpt.rage_you
with a as
(select
       "day" as time,
       substring ("day" ,1,7) as month,
substring ("day" ,1,4) as year,
       origin_type_state,
       cast(cnt_you as double) as cnt1_you ,
       cast(cnt_o_sc as double) as cnt1_o_sc
from hive.edu_dws.dws_customer_detail_cnt
where origin_type_state =1)
,b as (select
       time,
       month,year,
       origin_type_state,
  cnt1_o_sc,  cnt1_you,
(cnt1_o_sc/cnt1_you) as rage_day,
sum(cnt1_o_sc) over(partition by "month" ) as c_osc_m ,
sum(cnt1_you) over(partition by "month" ) as c_you_m,
sum(cnt1_o_sc) over(partition by "year" ) as c_osc_y,
sum(cnt1_you) over(partition by "year" ) as  c_you_y
from a)
 ,c as  (select * ,(c_osc_m/c_you_m) as rage_month,
       (c_osc_y/c_you_y) as rage_year from b)
select * from c
where rage_day>0;

