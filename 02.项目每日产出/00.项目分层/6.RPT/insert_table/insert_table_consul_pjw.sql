-- 总访问客户量
insert into hive.edu_rpt.user_cnt
with temp as (select
    distinct date_time,
     case when time_type = 'hour' then date_hour
          when time_type = 'day'  then year_month_day
          when time_type = 'month' then year_month
          when time_type = 'quarter' then quarter_code
          when time_type = 'year' then year_code
          else 'other' end as date_type,
    time_type,
    year_code,
    quarter_code,
    year_month,
    year_month_day,
    date_hour,
    user_nums
from hive.edu_dm.dm_web_chat_ems)
select
    distinct date_type,
    time_type,
    sum(user_nums) over(partition by date_type rows between unbounded preceding and unbounded following )as user_nums
from temp
order by date_type;

-- 地区独立访客热力图
insert into hive.edu_rpt.distri_cnt
with temp as (select
    distinct date_time,
     case when group_type = 'country' then country
          when group_type = 'province'  then province
          when group_type = 'city' then city
          else 'other' end as distr_type,
    time_type,
    group_type,
    country,
    province,
    city,
    user_nums
from hive.edu_dm.dm_web_chat_ems),
t1 as (select
    distinct distr_type,
    group_type,
    sum(user_nums) over(partition by distr_type rows between unbounded preceding and unbounded following )as user_nums,
    row_number() over (partition by distr_type rows between unbounded preceding and unbounded following) as rn
from temp
where (group_type = 'country' or group_type = 'province' or group_type = 'city') and (distr_type != '') and distr_type is not null
order by distr_type)
select
    distr_type,
    group_type,
    user_nums
from t1
where rn = 1;

-- 访客咨询率趋势
insert into edu_rpt.consul_rate_cnt
with temp as (select
    distinct date_time,
    case when group_type = 'province'  then province
          when group_type = 'city' then city
          else null end as distr_type,
    case when time_type = 'year' then year_code
         when time_type = 'quarter' then quarter_code
         when time_type = 'month' then year_month
         else null end as date_type,
    time_type,
    group_type,
    province,
    city,
    user_nums,
    consul_nums
from hive.edu_dm.dm_web_chat_ems),
t2 as (select
    distinct date_type,
    time_type,
    group_type,
    distr_type,
    sum(user_nums) over(partition by date_type,distr_type rows between unbounded preceding and unbounded following ) as user_nums,
    sum(consul_nums) over(partition by date_type,distr_type rows between unbounded preceding and unbounded following ) as consul_nums
from temp
where ((time_type = 'year') or (time_type = 'quarter') or (time_type = 'month')) and
      ((group_type = 'province') or (group_type = 'city') and
      distr_type != '')
order by date_type)
select
    date_type,
    time_type,
    group_type,
    distr_type,
    if(user_nums != 0, ((consul_nums * 1.00) / user_nums)*100,0) as consul_rate
from t2
where (distr_type is not null) and (distr_type != '');


-- 客户访问量和访客咨询率双轴趋势


-- 时间段访问客户量趋势
insert into edu_rpt.hour_cnt
with temp as (select
    distinct date_time,
    date_hour,
    user_nums
from hive.edu_dm.dm_web_chat_ems
where date_hour is not null)
select
    distinct date_hour,
    sum(user_nums) over(partition by date_hour rows between unbounded preceding and unbounded following )as user_nums
from temp
order by date_hour;

-- 来源渠道访问量占比
insert into edu_rpt.origin_cnt
with temp as (select
    distinct date_time,
    case when time_type = 'year' then year_code
         when time_type = 'quarter' then quarter_code
         when time_type = 'month' then year_month  end as date_type,
    time_type,
    origin_channel,
    user_nums
from hive.edu_dm.dm_web_chat_ems
where origin_channel is not null)
select
    distinct
    date_type,
    origin_channel,
    (sum(user_nums) over(partition by date_type,origin_channel rows between unbounded preceding and unbounded following )*1.00) / (sum(user_nums) over(partition by date_type rows between unbounded preceding and unbounded following )) as rate
from temp where time_type = 'year' or time_type = 'quarter' or time_type = 'month'
order by date_type;

-- 搜索来源访问量占比
insert into edu_rpt.source_cnt
with temp as (select
    distinct date_time,
    case when time_type = 'year' then year_code
         when time_type = 'quarter' then quarter_code
         when time_type = 'month' then year_month  end as date_type,
    time_type,
    seo_source,
    user_nums
from hive.edu_dm.dm_web_chat_ems
where (seo_source is not null) and (seo_source != ''))
select
    distinct
    date_type,
    seo_source,
    (sum(user_nums) over(partition by date_type,seo_source rows between unbounded preceding and unbounded following )*1.00) / (sum(user_nums) over(partition by date_type rows between unbounded preceding and unbounded following )) as rate
from temp where time_type = 'year' or time_type = 'quarter' or time_type = 'month'
order by date_type;

-- 活跃页面排行榜
insert into edu_rpt.from_url_cnt
with temp as (select
    distinct date_time,
    case when time_type = 'year' then year_code
         when time_type = 'quarter' then quarter_code
         when time_type = 'month' then year_month  end as date_type,
    time_type,
    from_url,
    user_nums
from hive.edu_dm.dm_web_chat_ems
where (from_url is not null) and (from_url != '')),
t2 as (select
    distinct
    time_type,
    date_type,
    from_url,
    sum(user_nums) over (partition by date_type,from_url rows between unbounded preceding and unbounded following )  as nums
from temp where time_type = 'year' or time_type = 'quarter' or time_type = 'month'
order by date_type,nums desc)
select
    time_type,
    date_type,
    from_url,
    nums,
    row_number() over (partition by date_type order by date_type,nums desc rows between unbounded preceding and unbounded following) as rn
from t2;






