-- 每天/每月/每年线上线下以及新老学员的意向用户个数
INSERT INTO edu_rpt.up_down
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       origin_type_state,
       clue_state_stat,
       customer_total
FROM edu_dm.dm_intention_data;
-- 每天/每月/每年各地区的线上线下以及新老学员的意向用户个数
INSERT INTO edu_rpt.area_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       area,
       origin_type_state,
       clue_state_stat,
       customer_total
FROM edu_dm.dm_intention_data
WHERE area IS NOT NULL ;
--每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10
INSERT INTO edu_rpt.subject_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       subject_name,
       origin_type_state,
       clue_state_stat,
       customer_total,
       rn_year_subject,
       rn_month_subject,
       rn_day_subject
FROM edu_dm.dm_intention_top10_data
WHERE subject_name IS NOT NULL;
-- 每天/每月/每年各校区线上线下以及新老学员的意向用户个数Top10
INSERT INTO edu_rpt.school_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       school_name,
       origin_type_state,
       clue_state_stat,
       customer_total,
       rn_year_school,
       rn_month_school,
       rn_day_school
FROM edu_dm.dm_intention_top10_data
WHERE school_name IS NOT NULL ;
-- 每天/每月/每年各来源渠道线上线下以及新老学员的意向用户个数
INSERT INTO edu_rpt.origin_type_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       origin_type,
       clue_state_stat,
       customer_total
FROM edu_dm.dm_intention_data
WHERE origin_type IS NOT NULL ;
-- 每天/每月/每年各咨询中心线上线下以及新老学员的意向用户个数
INSERT INTO edu_rpt.depart_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       depart_name,
       origin_type_state,
       clue_state_stat,
       customer_total
FROM edu_dm.dm_intention_data
WHERE depart_name IS NOT NULL ;
-- 每天线上线下及新老学员的有效线索个数
INSERT INTO edu_rpt.vail_clue_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       origin_type_state,
       clue_state_stat,
       c_vail
FROM edu_dm.dm_clue_data
WHERE c_vail IS NOT NULL
AND dayinfo IS NOT NULL ;
-- 每小时线上线下及新老学员的有效线索转化率 = 有效线索个数 / 总线索个数
INSERT INTO edu_rpt.vail_percent_class
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       hourinfo,
       origin_type_state,
       clue_state_stat,
       c_vail_percent
FROM edu_dm.dm_clue_data
WHERE  hourinfo IS NOT NULL ;