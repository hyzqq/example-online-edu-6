-- 每天/每月/每年线上线下以及新老学员的意向用户个数
DROP TABLE IF EXISTS edu_rpt.up_down;
CREATE TABLE IF NOT EXISTS edu_rpt.up_down(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:新用户,1:老用户',
    customer_total bigint COMMENT '意向用1户个数'
)comment '线上线下意向用户个数'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

-- 每天/每月/每年各地区的线上线下以及新老学员的意向用户个数
DROP TABLE IF EXISTS edu_rpt.area_class;
CREATE TABLE IF NOT EXISTS edu_rpt.area_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    area string COMMENT '地区',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:新用户,1:老用户',
    customer_total bigint COMMENT '意向用1户个数'
)comment '各地区意向用户个数'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
--每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10
DROP TABLE IF EXISTS edu_rpt.subject_class;
CREATE TABLE IF NOT EXISTS edu_rpt.subject_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    subject_name string COMMENT '学科',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:新用户,1:老用户',
    customer_total bigint COMMENT '意向用户个数',
    rn_year_subject bigint COMMENT '每年学科排名',
    rn_month_subject bigint COMMENT '每月学科排名',
    rn_day_subject bigint COMMENT '每日学科排名'
)comment '学科top10'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- 每天/每月/每年各校区线上线下以及新老学员的意向用户个数Top10
DROP TABLE IF EXISTS edu_rpt.school_class;
CREATE TABLE IF NOT EXISTS edu_rpt.school_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    school_name string COMMENT '校区',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:新用户,1:老用户',
    customer_total bigint COMMENT '意向用户个数',
    rn_year_school bigint COMMENT '每年校区排名',
    rn_month_school bigint COMMENT '每月校区排名',
    rn_day_school bigint COMMENT '每日校区排名'
)comment '校区top10'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- 每天/每月/每年各来源渠道线上线下以及新老学员的意向用户个数
DROP TABLE IF EXISTS edu_rpt.origin_type_class;
CREATE TABLE IF NOT EXISTS edu_rpt.origin_type_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    origin_type string COMMENT '来源渠道',
    clue_state_stat int COMMENT '0:新用户,1:老用户',
    customer_total bigint COMMENT '意向用户个数'
)comment '各来源渠道意向用户个数'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- 每天/每月/每年各咨询中心线上线下以及新老学员的意向用户个数
DROP TABLE IF EXISTS edu_rpt.depart_class;
CREATE TABLE IF NOT EXISTS edu_rpt.depart_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    depart_name string COMMENT '咨询中心名称',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:新用户,1:老用户',
    customer_total bigint COMMENT '意向用户个数'
)comment '各咨询中心意向用户个数'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- 每天线上线下及新老学员的有效线索个数
DROP TABLE IF EXISTS edu_rpt.vail_clue_class;
CREATE TABLE IF NOT EXISTS edu_rpt.vail_clue_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:老用户,1:新用户',
    c_vail bigint COMMENT '有效线索个数'
)comment '有效线索个数'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- 每小时线上线下及新老学员的有效线索转化率 = 有效线索个数 / 总线索个数
DROP TABLE IF EXISTS edu_rpt.vail_percent_class;
CREATE TABLE IF NOT EXISTS edu_rpt.vail_percent_class(
    yearinfo int COMMENT '年',
    monthinfo int COMMENT '月',
    dayinfo int COMMENT '日',
    hourinfo int COMMENT '小时',
    origin_type_state int COMMENT '0:线上,1:线下',
    clue_state_stat int COMMENT '0:老用户,1:新用户',
    c_vail_percent bigint COMMENT '有效线索转换率'
)comment '有效线索转换率'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');