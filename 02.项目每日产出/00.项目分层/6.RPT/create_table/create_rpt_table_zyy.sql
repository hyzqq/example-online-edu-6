drop table if exists edu_rpt.scpay;
create table if not exists edu_rpt.scpay
(
`month` string,
`year` string,
`time` string,
sc_name string,
cnt_sc bigint,
c_m bigint,
c_y bigint
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_rpt.scopay;
create table if not exists edu_rpt.scopay
(
`month` string,
`year` string,
`time` string,
sc_name string,
cnt_o_sc bigint,
c_m bigint,
c_y bigint
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_rpt.suopay;
create table if not exists edu_rpt.suopay
(
`month` string,
`year` string,
`time` string,
name string,
cnt_o_su bigint,
c_m bigint,
c_y bigint
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_rpt.scsuopay;
create table if not exists edu_rpt.scsuopay
(
`month` string,
`year` string,
`time` string,
name string,
sc_name string,
cnt_o_su bigint,
cnt_o_sc bigint,
su_m bigint,
su_y bigint,
sc_m bigint,
sc_y bigint

)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_rpt.quopay;
create table if not exists edu_rpt.quopay
(
`month` string,
`year` string,
`time` string,
origin_channel string,
cnt_qu bigint,
c_m bigint,
c_y bigint
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_rpt.ziopay;
create table if not exists edu_rpt.ziopay
(
`month` string,
`year` string,
`time` string,
depart_name string,
cnt_qu bigint,
c_m bigint,
c_y bigint
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists edu_rpt.rage_yi;
create table if not exists edu_rpt.rage_yi
(
`month` string,
`year` string,
`time` string,
origin_type_state int,
cnt1_o_sc double,
cnt1_yi double,
rage_day double,
c_yi_m double,
c_osc_m double,
c_yi_y double,
c_osc_y double,
rage_month double,
rage_year double
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');


drop table if exists edu_rpt.rage_you;
create table if not exists edu_rpt.rage_you
(
    `time` string,
`month` string,
`year` string,

origin_type_state int,
cnt1_o_sc double,
cnt1_you double,
rage_day double,
c_osc_m double,
c_you_m double,
c_osc_y double,
c_you_y double,
rage_month double,
rage_year double
)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');
