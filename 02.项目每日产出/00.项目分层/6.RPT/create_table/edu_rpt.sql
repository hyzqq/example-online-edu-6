--每天每个班级==各时间段==的正常出勤人数、出勤率
create table student_attendance
(
    class_id               int comment '班级id',
    studying_student_count int comment '在读班级人数',
    class_date                      date comment '上课日期',
    moring_attendance_cnt           int comment '上午出勤人数',
    moring_attendance_pre           int comment '上午出勤率',
    afternoon_attendance_cnt        int comment '中午出勤人数',
    afternoon_attendance_pre        int comment '中午出勤率',
    evening_attendance_cnt          int comment '晚上出勤人数',
    evening_attendance_pre          int comment '晚上出勤率'
)COMMENT '出勤情况'
ROW format delimited fields terminated BY '\t'
stored AS orc tblproperties ('orc.compress' = 'SNAPPY');
--每天各个班级各时间段的迟到人数、迟到率
create table student_arrive_late
(
    class_id               int comment '班级id',
    studying_student_count int comment '在读班级人数',
    class_date                      date comment '上课日期',
    moring_arrive_late_cnt          int comment '上午迟到人数',
    moring_arrive_late_pre          int comment '上午迟到率',
    afternoon_arrive_late_cnt       int comment '中午迟到人数',
    afternoon_arrive_late_pre       int comment '中午迟到率',
    evening_arrive_late_cnt         int comment '晚上迟到人数',
    evening_arrive_late_pre         int comment '晚上迟到率'
)COMMENT '请假情况'
ROW format delimited fields terminated BY '\t'
stored AS orc tblproperties ('orc.compress' = 'SNAPPY');

--每天各个班级各时间段的请假人数、请假率
create table student_take_off
(
    class_id               int comment '班级id',
    studying_student_count int comment '在读班级人数',
    class_date                      date comment '上课日期',
    moring_take_off_cnt             int comment '上午请假人数',
    moring_take_off_pre             int comment '上午请假率',
    afternoon_take_off_cnt          int comment '中午请假人数',
    afternoon_take_off_pre          int comment '中午请假率',
    evening_take_off_cnt            int comment '晚上请假人数',
    evening_take_off_pre            int comment '晚上请假率'
)COMMENT '请假情况'
ROW format delimited fields terminated BY '\t'
stored AS orc tblproperties ('orc.compress' = 'SNAPPY');
--每天各个班级各时间段的旷课人数、旷课率
create table student_volume_up_truancy
(
    class_id               int comment '班级id',
    studying_student_count int comment '在读班级人数',
    class_date                      date comment '上课日期',
    moring_volume_up_truancy_cnt    int comment '上午旷课人数',
    moring_volume_up_truancy_pre    int comment '上午旷课率',
    afternoon_volume_up_truancy_cnt int comment '中午旷课人数',
    afternoon_volume_up_truancy_pre int comment '中午旷课率',
    evening_volume_up_truancy_cnt   int comment '晚上旷课人数',
    evening_truancy_pre             int comment '晚上旷课率'
)COMMENT '旷课情况'
ROW format delimited fields terminated BY '\t'
stored AS orc tblproperties ('orc.compress' = 'SNAPPY');