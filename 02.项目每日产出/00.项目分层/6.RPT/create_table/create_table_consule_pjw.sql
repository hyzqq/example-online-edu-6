-- 总访问客户量建表语句
create table if not exists edu_rpt.user_cnt(
    date_type string comment '时间类型',
    time_type string comment '分组标记',
    user_nums bigint
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 地区独立访客热力图建表语句
drop table if exists edu_rpt.distri_cnt;
create table if not exists edu_rpt.distri_cnt(
    distr_type string comment '地区类型',
    group_type string comment '分组标记',
    user_nums bigint
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 访客咨询率趋势建表语句
drop table if exists edu_rpt.consul_rate_cnt;
create table if not exists edu_rpt.consul_rate_cnt(
    date_type string comment '时间',
    time_type string comment '时间粒度',
    group_type string comment '地区粒度',
    distr_type string comment '地区',
    consul_rate decimal(32,2) comment '咨询率'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 时间段访问客户量趋势建表语句

drop table edu_rpt.hour_cnt;
create table if not exists edu_rpt.hour_cnt(
    date_hour string comment '天-小时',
    user_nums bigint
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');
-- 来源渠道访问量占比建表语句

drop table edu_rpt.origin_cnt;
create table if not exists edu_rpt.origin_cnt(
    date_type string comment '时间类型',
    origin_channel string comment '来源渠道',
    rate decimal(22,2) comment '比率'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 搜索来源访问量占比建表语句
drop table edu_rpt.source_cnt;
create table if not exists edu_rpt.source_cnt(
    date_type string comment '时间类型',
    seo_source string comment '搜索来源',
    rate decimal(22,2) comment '比率'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

-- 活跃页面排行榜建表语句
drop table edu_rpt.from_url_cnt;
create table if not exists edu_rpt.from_url_cnt(
    time_type string comment '时间标记',
    date_type string comment '时间类型',
    from_url string comment '来源页面',
    nums    bigint comment '用户数量',
    rn decimal(22,2) comment '排名'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

