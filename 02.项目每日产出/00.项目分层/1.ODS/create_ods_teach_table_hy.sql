-- 创建ods层在读学员人数信息表
drop table if exists edu_ods.class_studying_student_count;
CREATE TABLE edu_ods.class_studying_student_count
(
    id INT,
    school_id INT COMMENT '校区id',
    subject_id INT COMMENT '学科id',
    class_id INT COMMENT '班级id',
    studying_student_count INT COMMENT '在读班级人数',
    studying_date DATE  COMMENT '在读日期'

)comment '在读学员人数信息表'
partitioned by (export_date string)
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定ORC格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用Snappy压缩


-- 创建ods层学生打卡信息表
drop table if exists edu_ods.tbh_student_signin_record;
CREATE TABLE edu_ods.tbh_student_signin_record
(
    id INT COMMENT '主键id',
    normal_class_flag INT COMMENT '是否正课 1 正课 2 自习',
    time_table_id INT COMMENT '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id INT COMMENT '班级id',
    student_id INT COMMENT '学员id',
    signin_time TIMESTAMP COMMENT '签到时间', -- DATETIME 在 Hive 中用 TIMESTAMP 类型表示
    signin_date DATE COMMENT '签到日期',
    inner_flag INT COMMENT '内外网标志 0 外网 1 内网',
    signin_type INT COMMENT '签到类型 1 心跳打卡 2 老师补卡',
    share_state INT COMMENT '共享屏幕状态 0 否 1是 在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0',
    inner_ip STRING COMMENT '内网ip地址'-- VARCHAR 在 Hive 中使用 STRING 类型

)
COMMENT '学生打卡信息表'
PARTITIONED BY (export_date string) -- 添加分区字段
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩

--创建edu.ods层班级作息时间表
drop table if exists edu_ods.tbh_class_time_table;
CREATE TABLE edu_ods.tbh_class_time_table
(
    id INT COMMENT '主键id',
    class_id INT COMMENT '班级id',
    morning_template_id INT COMMENT '上午出勤模板id',
    morning_begin_time STRING COMMENT '上午开始时间',  -- Hive中TIME类型通常使用STRING表示
    morning_end_time STRING COMMENT '上午结束时间',
    afternoon_template_id INT COMMENT '下午出勤模板id',
    afternoon_begin_time STRING COMMENT '下午开始时间',
    afternoon_end_time STRING COMMENT '下午结束时间',
    evening_template_id INT COMMENT '晚上出勤模板id',
    evening_begin_time STRING COMMENT '晚上开始时间',
    evening_end_time STRING COMMENT '晚上结束时间',
    use_begin_date DATE COMMENT '使用开始日期',
    use_end_date DATE COMMENT '使用结束日期',
    create_time TIMESTAMP COMMENT '创建时间',  -- DATETIME 在 Hive 中用 TIMESTAMP 表示
    create_person INT COMMENT '创建人',
    remark STRING COMMENT '备注'  -- VARCHAR(500) 在 Hive 中使用 STRING 类型

)comment '班级作息时间表'
PARTITIONED BY (export_date string) -- 添加分区字段
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩

-- 创建edu.ods层班级排课信息表
drop table if exists edu_ods.course_table_upload_detail;
CREATE TABLE edu_ods.course_table_upload_detail
(
    id INT COMMENT 'id',  -- 去除 AUTO_INCREMENT，假设在数据加载时已保证其唯一性
    base_id INT COMMENT '课程主表id',
    class_id INT COMMENT '班级id',
    class_date DATE COMMENT '上课日期',
    content STRING COMMENT '课程内容',  -- VARCHAR 转为 STRING
    teacher_id INT COMMENT '老师id',
    teacher_name STRING COMMENT '老师名字',  -- VARCHAR 转为 STRING
    job_number STRING COMMENT '工号',  -- VARCHAR 转为 STRING
    classroom_id INT COMMENT '教室id',
    classroom_name STRING COMMENT '教室名称',  -- VARCHAR 转为 STRING
    is_outline INT COMMENT '是否大纲 0 否 1 是',
    class_mode INT COMMENT '上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播',
    is_stage_exam INT COMMENT '是否阶段考试（0：否 1：是）',
    is_pay INT COMMENT '代课费（0：无 1：有）',
    tutor_teacher_id INT COMMENT '晚自习辅导老师id',
    tutor_teacher_name STRING COMMENT '辅导老师姓名',  -- VARCHAR 转为 STRING
    tutor_job_number STRING COMMENT '晚自习辅导老师工号',  -- VARCHAR 转为 STRING
    is_subsidy INT COMMENT '晚自习补贴（0：无 1：有）',
    answer_teacher_id INT COMMENT '答疑老师id',
    answer_teacher_name STRING COMMENT '答疑老师姓名',  -- VARCHAR 转为 STRING
    answer_job_number STRING COMMENT '答疑老师工号',  -- VARCHAR 转为 STRING
    remark STRING COMMENT '备注',  -- VARCHAR 转为 STRING
    create_time TIMESTAMP COMMENT '创建时间'   -- DATETIME 转为 TIMESTAMP

)comment '班级排课信息表'
PARTITIONED BY (export_date string) -- 添加分区字段
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩

-- 创建edu.ods层学生请假申请表
drop table edu_ods.student_leave_apply;
CREATE TABLE edu_ods.student_leave_apply
(
    id INT COMMENT '主键id',  -- 去除 AUTO_INCREMENT，假设在数据加载时已保证其唯一性
    class_id INT COMMENT '班级id',
    student_id INT COMMENT '学员id',
    audit_state INT  COMMENT '审核状态 0 待审核 1 通过 2 不通过',
    audit_person INT COMMENT '审核人',
    audit_time TIMESTAMP COMMENT '审核时间',  -- DATETIME 转为 TIMESTAMP
    audit_remark STRING COMMENT '审核备注',  -- VARCHAR 转为 STRING
    leave_type INT COMMENT '请假类型  1 请假 2 销假',
    leave_reason INT COMMENT '请假原因  1 事假 2 病假',
    begin_time TIMESTAMP COMMENT '请假开始时间',  -- DATETIME 转为 TIMESTAMP
    begin_time_type INT COMMENT '1：上午 2：下午',
    end_time TIMESTAMP COMMENT '请假结束时间',  -- DATETIME 转为 TIMESTAMP
    end_time_type INT COMMENT '1：上午 2：下午',
    days FLOAT COMMENT '请假/已休天数',
    cancel_state INT  COMMENT '撤销状态  0 未撤销 1 已撤销',
    cancel_time TIMESTAMP COMMENT '撤销时间',  -- DATETIME 转为 TIMESTAMP
    old_leave_id INT COMMENT '原请假id，只有leave_type =2 销假的时候才有',
    leave_remark STRING COMMENT '请假/销假说明',  -- VARCHAR 转为 STRING
    valid_state INT  COMMENT '是否有效（0：无效 1：有效）',
    create_time TIMESTAMP COMMENT '创建时间'  -- DATETIME 转为 TIMESTAMP

)comment '学生请假申请表'
PARTITIONED BY (export_date string) -- 添加分区字段
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩