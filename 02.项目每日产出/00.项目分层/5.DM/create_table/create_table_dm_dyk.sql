--创建一个表，用来存放计算出来的意向用户个数数据
CREATE TABLE IF NOT EXISTS edu_dm.dm_intention_data(
    yearinfo int COMMENT '创建年',
    monthinfo int COMMENT '创建月',
    dayinfo int COMMENT'创建日',
    hourinfo int COMMENT '创建小时',
    area string COMMENT '地区',
    origin_type string COMMENT '数据来源',
    origin_type_state int COMMENT '0:线上，1：线下',
    depart_name string COMMENT '部门名称',
    clue_state_stat int COMMENT '0:新用户，1：老用户',
    customer_total  bigint COMMENT '用户人数'
)comment '客户意向人数数据表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

-- 创建另一个表用来存放各学科，各校区意向用户个数的TOP10
DROP TABLE IF EXISTS edu_dm.dm_intention_top10_data;
CREATE TABLE IF NOT EXISTS edu_dm.dm_intention_top10_data(
    yearinfo int COMMENT '创建年',
    monthinfo int COMMENT '创建月',
    dayinfo int COMMENT'创建日',
    subject_name string COMMENT '学科名称',
    school_name string COMMENT '校区名称',
    origin_type_state int COMMENT '0:线上，1：线下',
    clue_state_stat int COMMENT '0:新用户，1：老用户',
    customer_total  bigint COMMENT '用户人数',
    rn_year_subject bigint COMMENT '学科年排名',
    rn_month_subject bigint COMMENT '学科年月排名',
    rn_day_subject bigint COMMENT '学科年月日排名',
     rn_year_school bigint COMMENT '校区年排名',
    rn_month_school bigint COMMENT '校区年月排名',
    rn_day_school bigint COMMENT '校区年月日排名'
)comment '客户意向人数top10数据表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

-- 再创建一个表用来存放有效线索个数，有效线索转换率
DROP TABLE IF EXISTS edu_dm.dm_clue_data;
CREATE TABLE IF NOT EXISTS edu_dm.dm_clue_data(
    yearinfo int COMMENT '创建年',
    monthinfo int COMMENT '创建月',
    dayinfo int COMMENT '创建日',
    hourinfo int COMMENT'创建小时',
    origin_type_state int COMMENT'0:线上,1:线下',
    clue_state_stat int COMMENT '0:老用户,1:新用户',
    c_vail bigint COMMENT '有效线索个数',
    c_vail_percent bigint COMMENT '有效线索转换率'
)comment '客户线索数据表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');