--todo 每月每年线上线下各个校区学科、来源渠道、咨询中心报名人数，线上的有效线索报名转换率，线上线下意向人数
drop table if exists edu_dm.dm_customer_detail_cnt_m_y;
create table edu_dm.dm_customer_detail_cnt_m_y
(
 time_type string,
appeal_status int ,
payment_state int ,
itcast_school_id int ,
sc_name string,
itcast_subject_id int,
name string,
origin_type_state int,
origin_channel string,
tdepart_id int,
depart_name string,
`year` string,
`month`  string,
c_o_sc_su bigint,
c_o_sc bigint,
c_sc bigint,
c_o_su bigint,
c_qu bigint,
c_zi bigint,
c_yi bigint,
c_you bigint

)comment '每天线上线下各个校区各个学科的报名人数、意向人数、有效线索人数'
ROW format delimited fields terminated BY '\t'
stored AS orc tblproperties ('orc.compress' = 'SNAPPY');
