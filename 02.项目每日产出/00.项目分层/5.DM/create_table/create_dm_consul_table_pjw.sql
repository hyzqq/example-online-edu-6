drop table if exists edu_dm.dm_web_chat_ems;
create table if not exists edu_dm.dm_web_chat_ems
(
    date_time      string comment '统计日期,用来标记统计日期',
    --时间粒度标记
    time_type      string comment '统计时间维度：year、quarter、month、date(就是天day)、hour',
    --时间粒度字段
    year_code      string comment '年,如2014',
    quarter_code   string comment '季度',
    year_month     string comment '年月,如201401',
    year_month_day string comment '天，如20240503',
    date_hour      string comment '小时',

    -- 维度
    group_type     string comment '分组类型：',
    country        string comment '所在国家',
    province       string comment '省',
    city           string comment '城市',
    origin_channel string comment '来源渠道(广告)',
    seo_source     string comment '搜索来源',
    from_url       string comment '会话来源页面',
    --   =======统计=======
    user_nums       bigint comment '总访问用户量',
    ip_nums         bigint comment '总访问ip个数',
    session_nums    bigint comment '总访问session个数',
    consul_nums     bigint comment '咨询人数'
)
    partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');



