-- 将各维度意向个数插入dm数据表dm_intention_data
INSERT INTO edu_dm.dm_intention_data
-- 指定维度意向个数
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       hourinfo,
       area,
       origin_type,
       origin_type_state,
       depart_name,
       clue_state_stat,
       count( distinct customer_id) as customer_total
FROM edu_dwb.intention_details
GROUP BY
    GROUPING SETS (
  -- 每天/每月/每年线上线下以及新老学员的意向用户个数
    (yearinfo, origin_type_state, clue_state_stat),-- 每年
    (yearinfo,monthinfo,origin_type_state,clue_state_stat),    --每年每月
    (yearinfo,monthinfo,dayinfo,origin_type_state,clue_state_stat),    -- 每年每月每日
    (yearinfo,monthinfo,dayinfo,hourinfo,origin_type_state,clue_state_stat),-- 每年每月每日每小时
-- 每天/每月/每年各地区的线上线下以及新老学员的意向用户个数
    (yearinfo,area,origin_type_state,clue_state_stat),    -- 每年
    (yearinfo,monthinfo,area,origin_type_state,clue_state_stat),    --每年每月
    (yearinfo,monthinfo,dayinfo,area,origin_type_state,clue_state_stat),    -- 每年每月每日
    (yearinfo,monthinfo,dayinfo,hourinfo,area,origin_type_state,clue_state_stat),-- 每年每月每日每小时
-- 每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10

-- 每天/每月/每年各校区线上线下以及新老学员的意向用户个数Top10

--每天/每月/每年各来源渠道线上线下以及新老学员的意向用户个数
    (yearinfo,origin_type,clue_state_stat),    -- 每年
    (yearinfo,monthinfo,origin_type,clue_state_stat),    --每年每月
    (yearinfo,monthinfo,dayinfo,origin_type,clue_state_stat),    -- 每年每月每日
    (yearinfo,monthinfo,dayinfo,hourinfo,origin_type,clue_state_stat),-- 每年每月每日每小时
-- 每天/每月/每年各咨询中心线上线下以及新老学员的意向用户个数
    (yearinfo,depart_name,origin_type_state,clue_state_stat),    -- 每年
    (yearinfo,monthinfo,depart_name,origin_type_state,clue_state_stat),    --计每年每月
    (yearinfo,monthinfo,dayinfo,depart_name,origin_type_state,clue_state_stat),    -- 每年每月每日
    (yearinfo,monthinfo,dayinfo,hourinfo,depart_name,origin_type_state,clue_state_stat)--每年每月每日每小时
    )
ORDER BY yearinfo,monthinfo,dayinfo,hourinfo,area,origin_type,depart_name;



--将学科和校区意向个数top10数据插入dm数据表dm_intention_top10_data
INSERT INTO edu_dm.dm_intention_top10_data

--指定维度意向用户各个数排名top10
((SELECT
       yearinfo,
       monthinfo,
       dayinfo,
        subject_name,
        NULL AS school_name,
       origin_type_state,
       clue_state_stat,
       count( distinct customer_id) as customer_total,
-- 每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10
       -- 每年
       CASE WHEN GROUPING (yearinfo, subject_name,origin_type_state, clue_state_stat)=0
                     AND GROUPING (monthinfo)=1
                     AND GROUPING (dayinfo)=1
                THEN row_number()over(PARTITION BY yearinfo,monthinfo,dayinfo,origin_type_state ORDER BY count(DISTINCT  customer_id) DESC)
                ELSE NULL END AS rn_year_subject,
       -- 每年每月
       CASE WHEN GROUPING (yearinfo, monthinfo,subject_name,origin_type_state, clue_state_stat)=0
                     AND GROUPING (dayinfo) = 1
                THEN row_number()over(PARTITION BY yearinfo,monthinfo,dayinfo,origin_type_state ORDER BY count(DISTINCT  customer_id) DESC)
                ELSE NULL END AS rn_month_subject,
       -- 每年每月每日
       CASE WHEN GROUPING (yearinfo, monthinfo,dayinfo,subject_name,origin_type_state, clue_state_stat)=0
                THEN row_number()over(PARTITION BY yearinfo,monthinfo,dayinfo,origin_type_state ORDER BY count(DISTINCT  customer_id) DESC)
                ELSE NULL END AS rn_day_subject,
        NULL AS rn_year_school,
        NULL AS rn_month_school,
        NULL AS rn_day_school
FROM edu_dwb.intention_details
WHERE subject_name IS NOT null AND school_name IS NOT null
GROUP  BY
GROUPING  SETS (
  -- 每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10
  (yearinfo, subject_name,origin_type_state, clue_state_stat),--每年
  (yearinfo, monthinfo,subject_name,origin_type_state, clue_state_stat),--每年每月
  (yearinfo, monthinfo,dayinfo,subject_name,origin_type_state, clue_state_stat)--每年每月每日
)
UNION ALL
-- 每天/每月/每年各校区线上线下以及新老学员的意向用户个数Top10
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       NULL AS subject_name,
        school_name,
       origin_type_state,
       clue_state_stat,
       count( distinct customer_id) as customer_total,
       NULL as rn_year_subject,
       NULL AS rn_month_subject,
       NULL AS rn_day_subject,
-- 每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10
       -- 每年
       CASE WHEN GROUPING (yearinfo, school_name,origin_type_state, clue_state_stat)=0
                     AND GROUPING (monthinfo)=1
                     AND GROUPING (dayinfo)=1
                THEN row_number()over(PARTITION BY yearinfo,monthinfo,dayinfo,origin_type_state ORDER BY count(DISTINCT  customer_id) DESC)
                ELSE NULL END AS rn_year_school,
       -- 每年每月
       CASE WHEN GROUPING (yearinfo, monthinfo,school_name,origin_type_state, clue_state_stat)=0
                     AND GROUPING (dayinfo) = 1
                THEN row_number()over(PARTITION BY yearinfo,monthinfo,dayinfo,origin_type_state ORDER BY count(DISTINCT  customer_id) DESC)
                ELSE NULL END AS rn_month_school,
       -- 每年每月每日
       CASE WHEN GROUPING (yearinfo, monthinfo,dayinfo,school_name,origin_type_state, clue_state_stat)=0
                THEN row_number()over(PARTITION BY yearinfo,monthinfo,dayinfo,origin_type_state ORDER BY count(DISTINCT  customer_id) DESC)
                ELSE NULL END AS rn_day_school
FROM edu_dwb.intention_details
WHERE subject_name IS NOT null AND school_name IS NOT null
GROUP  BY
GROUPING  SETS (
-- 每天/每月/每年各校区线上线下以及新老学员的意向用户个数Top10
  (yearinfo, school_name,origin_type_state, clue_state_stat),   --每年
  (yearinfo, monthinfo,school_name,origin_type_state, clue_state_stat), --每月
  ( yearinfo, monthinfo,dayinfo,school_name,origin_type_state, clue_state_stat) --每日
))
ORDER BY yearinfo,monthinfo,dayinfo);


--插入线索个数数据和线索转换率到表dm_clue_data
INSERT INTO edu_dm.dm_clue_data
-- 每天线上线下及新老学员的有效线索个数
((SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       NULL AS hourinfo,
        origin_type_state,
       clue_state_stat,
       COUNT (if(appeal_status=1,1,NULL )) AS c_vail,
       NULL AS c_vail_percent
FROM edu_dwb.clue_details
GROUP BY yearinfo,monthinfo,dayinfo,origin_type_state,clue_state_stat,appeal_status
UNION ALL
-- 每小时线上线下及新老学员的有效线索转化率 = 有效线索个数 / 总线索个数
SELECT
       yearinfo,
       monthinfo,
       dayinfo,
       hourinfo,
       origin_type_state,
       clue_state_stat,
       NULL AS c_vail,
       COUNT (if(appeal_status=1,1,NULL ))/if(COUNT (appeal_status)=0,1,COUNT (appeal_status)) AS c_vail_percent
FROM edu_dwb.clue_details
GROUP BY yearinfo,monthinfo,dayinfo,hourinfo,origin_type_state,clue_state_stat,appeal_status )
ORDER BY c_vail DESC ,c_vail_percent DESC );