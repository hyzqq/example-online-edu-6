--todo 每月每年线上线下各个校区学科、来源渠道、咨询中心报名人数，线上的有效线索报名转换率，线上线下意向人数
insert into hive.edu_dm.dm_customer_detail_cnt_m_y
with a as (
select
--维度
"day",--天
substring ("day",1,7) as "month",--月
substring ("day",1,4) as "year" ,--年
itcast_school_id,sc_name,--学校
itcast_subject_id,name,--学科
origin_type_state,--线上线下,1线上，0为线下
origin_channel,--来源渠道
tdepart_id,depart_name,--咨询中心
group_type,--分组标记
cnt_o_sc_su ,
cnt_o_sc ,
cnt_sc ,
cnt_o_su ,
cnt_qu ,
cnt_zi ,
cnt_yi ,
cnt_you,
--统计
appeal_status,
payment_state
    from hive.edu_dws.dws_customer_detail_cnt
), b as
(select *,
--分组去重
row_number() over(partition by "month") as id_rn1,
row_number() over(partition by a."year") as id_rn2,
row_number() over(partition by a."month",a.itcast_school_id,a.sc_name,payment_state) as id_rn3,
row_number() over(partition by a."month",a.origin_type_state,a.itcast_school_id,a.sc_name,payment_state) as id_rn4,
row_number() over(partition by a."month",a.origin_type_state,a.itcast_subject_id,a.name,payment_state) as id_rn5,
row_number() over(partition by a."month",a.origin_type_state,a.itcast_school_id,a.sc_name,a.itcast_subject_id,a.name,payment_state)  as id_rn6,
row_number() over(partition by a."month",a.origin_type_state,a.origin_channel,payment_state) as id_rn7,
row_number() over(partition by a."month",a.origin_type_state,a.tdepart_id,a.depart_name,payment_state) as id_rn8,
row_number() over(partition by a."month",a.origin_type_state) as id_rn9,
row_number() over(partition by a."year",a.itcast_school_id,a.sc_name,payment_state) as id_rn10,
row_number() over(partition by a."year",a.origin_type_state,a.itcast_school_id,a.sc_name,payment_state) as id_rn11,
row_number() over(partition by a."year",a.origin_type_state,a.itcast_subject_id,a.name,payment_state) as id_rn12,
row_number() over(partition by a."year",a.origin_type_state,a.itcast_school_id,a.sc_name,a.itcast_subject_id,a.name,payment_state)  as id_rn13,
row_number() over(partition by a."year",a.origin_type_state,a.origin_channel,payment_state) as id_rn14,
row_number() over(partition by a."year",a.origin_type_state,a.tdepart_id,a.depart_name,payment_state) as id_rn15,
row_number() over(partition by a."year",a.origin_type_state) as id_rn16,
row_number() over(partition by a."year",a.origin_type_state,appeal_status) as id_rn17,
row_number() over(partition by a."year",a.origin_type_state,appeal_status) as id_rn18
from a
    )
, c as (select
case when grouping ("month","year")=0
    then "month"
    when grouping ("year")=0
        then "year"
              else 'other' end as time_type,

case when grouping (appeal_status) =0
then appeal_status
else null end as appeal_status,
case when grouping (payment_state)=0
then payment_state
else null end as payment_state ,
case when grouping (itcast_school_id) =0
then itcast_school_id
else null end as itcast_school_id,
case when grouping (itcast_school_id) =0
then sc_name
else null end as sc_name,
case when grouping (itcast_subject_id) =0
then itcast_subject_id
else null end as itcast_subject_id,
case when grouping (itcast_subject_id) =0
then name
else null end as name,
case when grouping (origin_type_state) =0
then origin_type_state
else null end as origin_type_state,
case when grouping (origin_channel) =0
then origin_channel
else null end as origin_channel,
case when grouping (tdepart_id) =0
then tdepart_id
else null end as tdepart_id,
case when grouping (tdepart_id) =0
then depart_name
else null end as depart_name,

"year",
"month",
sum(cnt_o_sc_su) as c_o_sc_su,
sum(cnt_o_sc) as c_o_sc,
sum(cnt_sc) as c_sc,
sum(cnt_o_su) as c_o_su,
sum(cnt_qu) as c_qu,
sum(cnt_zi) as c_zi,
sum(cnt_yi) as c_yi,
sum(cnt_you) as c_you

from b
group by
grouping sets (
("month","year"),
("year"),
("month",group_type),
("year",group_type),
("month",group_type,itcast_school_id,sc_name,payment_state),
("month",group_type,origin_type_state,itcast_school_id,sc_name,payment_state,cnt_o_sc),
("month",group_type,origin_type_state,itcast_subject_id,name,payment_state),
("month",group_type,origin_type_state,itcast_school_id,sc_name,itcast_subject_id,name,payment_state),
("month",group_type,origin_type_state,origin_channel,payment_state),
("month",group_type,origin_type_state,tdepart_id,depart_name,payment_state),
("month",group_type,origin_type_state),
("month",group_type,origin_type_state,appeal_status),

("year",group_type,itcast_school_id,sc_name,payment_state),
("year",group_type,origin_type_state,itcast_school_id,sc_name,payment_state),
("year",group_type,origin_type_state,itcast_subject_id,name,payment_state),
("year",group_type,origin_type_state,itcast_school_id,sc_name,itcast_subject_id,name,payment_state),
("year",group_type,origin_type_state,origin_channel,payment_state),
("year",group_type,origin_type_state,tdepart_id,depart_name,payment_state),
("year",group_type,origin_type_state),
("year",group_type,origin_type_state,appeal_status)
))
select
      distinct *
from c
where time_type<>'other';

