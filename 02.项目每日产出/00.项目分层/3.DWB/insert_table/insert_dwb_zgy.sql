--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;


insert into edu_dwb.student_detail
select
             a.id,
             a.normal_class_flag,
             a.time_table_id,
             a.class_id,
             a.student_id,
             a.signin_time,
             a.signin_date,
             a.share_state,
             a.inner_ip,

             b.base_id,
             b.class_date,
             b.content,

             c.morning_template_id,
             c.morning_begin_time,
             c.morning_end_time,
             c.afternoon_template_id,
             c.afternoon_begin_time,
             c.afternoon_end_time,
             c.evening_template_id,
             c.evening_begin_time,
             c.evening_end_time,
             c.use_begin_date,
             c.use_end_date,

             d.school_id,
             d.studying_student_count,
             d.studying_date,
            substring(a.end_date,1,10) as dt
from edu_dwd.fact_tbh_student_signin_record as a
       left join edu_dwd.dim_course_table_upload_detail as b
        on a.signin_date=b.class_date and a.class_id=b.class_id
        left join edu_dwd.dim_tbh_class_time_table as c
        on a.time_table_id=c.id
        left join edu_dwd.dim_class_studying_student_count as d
        on b.class_id=d.class_id and b.class_date=d.studying_date
        where b.class_date is not null
        and b.class_date>=c.use_begin_date
        and b.class_date<=c.use_end_date;

insert into edu_dwb.leave_detail
select
              a.class_id,
              a.old_leave_id as leave_student_id,
              a.audit_state,
              a.audit_time,
              a.leave_type,
              a.leave_reason,
              a.begin_time,
              a.begin_time_type,
              a.end_time,
              a.end_time_type,
              a.days,
              a.valid_state,
              a.create_time,
              a.end_date,

             b.morning_template_id,
             b.morning_begin_time,
             b.morning_end_time,
             b.afternoon_template_id,
             b.afternoon_begin_time,
             b.afternoon_end_time,
             b.evening_template_id,
             b.evening_begin_time,
             b.evening_end_time,
             b.use_begin_date,
             b.use_end_date,

             c.base_id,
             c.class_date,
             c.content,

             d.school_id,
             d.studying_student_count,
             d.studying_date,
             substring(a.end_date,1,10) as dt
from edu_dwd.fact_student_leave_apply as a
left join edu_dwd.dim_tbh_class_time_table as b
on a.class_id=b.class_id
left join edu_dwd.dim_course_table_upload_detail as c
on a.class_id=c.class_id
left join edu_dwd.dim_class_studying_student_count as d
on  c.class_id=d.class_id and c.class_date=d.studying_date
where a.begin_time<=c.class_date and a.end_time>=c.class_date;