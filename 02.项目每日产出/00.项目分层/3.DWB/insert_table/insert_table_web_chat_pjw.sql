insert overwrite table edu_dwb.dwb_web_chat_ems_detail partition (dt)
select
    a.id,
    substr(b.create_date_time,1,19) as create_date_time,
    a.session_id,
    a.sid,
    substr(b.create_time,1,19) as create_time,
    b.date_hour,
    a.seo_source,
    a.ip,
    c.area,
    c.country,
    c.province,
    c.city,
    a.origin_channel,
    a.msg_count,
    a.browser_name,
    a.os_info,
    a.from_url,
    '9999-99-99' as endtime,
    substr(dt,1,10) as dt
from edu_dwd.dwd_fact_web_chat_ems a
     left join edu_dwd.dwd_dim_date b on a.id = b.id
     left join edu_dwd.dwd_dim_area c on a.id = c.id
where sid is not null and sid != '';