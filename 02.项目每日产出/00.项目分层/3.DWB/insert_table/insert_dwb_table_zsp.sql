--分区
SET
hive.exec.dynamic.partition=true;
SET
hive.exec.dynamic.partition.mode=nonstrict;
set
hive.exec.max.dynamic.partitions.pernode=10000;
set
hive.exec.max.dynamic.partitions=100000;
set
hive.exec.max.created.files=150000;


set
hive.auto.convert.join=false;
--考勤出勤主题宽表


insert into table edu_dwb.tbh_student_signin_record_detail partition (dt)
select t1.id,
       normal_class_flag,
       time_table_id,
       t1.class_id,
       student_id,
       signin_time,
       signin_date,
       share_state,
       morning_template_id,
       morning_begin_time,
       morning_end_time,
       afternoon_template_id,
       afternoon_begin_time,
       afternoon_end_time,
       evening_template_id,
       evening_begin_time,
       evening_end_time,
       use_begin_date,
       use_end_date,
       base_id,
       class_date,
       content,
       school_id,
       studying_student_count,
       studying_date,
       t1.dt
from edu_dwd.fact_tbh_student_signin_record t1
         left join edu_dwd.dim_tbh_class_time_table t2 on t1.time_table_id = t2.id
         left join edu_dwd.dim_course_table_upload_detail t3
                   on t1.class_id = t3.class_id and t1.signin_date = t3.class_date
         left join edu_dwd.dim_class_studying_student_count t4
                   on t1.class_id = t4.class_id and t1.signin_date = t4.studying_date
where class_date between use_begin_date and use_end_date;

--请假宽表
insert into table edu_dwb.act_student_leave_apply_detail partition (dt)
select t1.id,
       t1.class_id,
       t1.student_id,
       t1.audit_state,
       t1.begin_time,
       t1.begin_time_type,
       t1.end_time,
       t1.end_time_type,
       t1.valid_state,
       t4.id,
       t4.class_id,
       t4.studying_student_count,
       t4.studying_date,
       t3.id,
       t3.class_id,
       t3.class_date,
       t3.content,
       t2.id,
       t2.class_id,
       t2.morning_begin_time,
       t2.morning_end_time,
       t2.afternoon_begin_time,
       t2.afternoon_end_time,
       t2.evening_begin_time,
       t2.evening_end_time,
       t2.use_begin_date,
       t2.use_end_date,
       t1.dt
from edu_dwd.fact_student_leave_apply t1
         left join
     edu_dwd.dim_tbh_class_time_table t2
     on t1.class_id = t2.class_id
         left join
     edu_dwd.dim_course_table_upload_detail t3
     on t3.class_id = t1.class_id
         left join
     edu_dwd.dim_class_studying_student_count t4
     on t4.class_id = t3.class_id and t4.studying_date = t3.class_date
where t1.audit_state = '通过'
  and t3.class_date >= substr(t1.begin_time, 1, 10)
  and t3.class_date <= substr(t1.end_time, 1, 10);




