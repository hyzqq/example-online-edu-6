--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
insert overwrite table edu_dwb.tbh_student_signin_record_detail partition (start_date)
select
-- 学生打卡记录表fact_tbh_student_signin_record
ftssr.id,
ftssr.time_table_id,
ftssr.class_id,
ftssr.student_id,
ftssr.signin_time,
ftssr.signin_date,
ftssr.share_state,
ftssr.end_date,
dcssc.id,
dcssc.class_id,
dcssc.studying_student_count,
dcssc.studying_date,
dctud.id,
dctud.class_id,
dctud.class_date,
dctud.content,
dtctt.id,
dtctt.class_id,
dtctt.morning_begin_time,
dtctt.morning_end_time,
dtctt.afternoon_begin_time,
dtctt.afternoon_end_time,
dtctt.evening_begin_time,
dtctt.evening_end_time,
dtctt.use_begin_date,
dtctt.use_end_date,
start_date
from edu_dwd.fact_tbh_student_signin_record ftssr
         left join edu_dwd.dim_course_table_upload_detail dctud
                   on ftssr.signin_date = dctud.class_date and ftssr.class_id = dctud.class_id  -- 通过打卡日期和上课日期关联以及班级id
         left join edu_dwd.dim_tbh_class_time_table dtctt
                   on ftssr.time_table_id = dtctt.id
         left join edu_dwd.dim_class_studying_student_count dcssc
                   on dcssc.class_id = dctud.class_id and dcssc.studying_date = dctud.class_date
where dctud.class_date >= dtctt.use_begin_date
  and dctud.class_date <= dtctt.use_end_date
  and  content is not null and content != '开班典礼' and class_date is not null;
  ;




INSERT INTO TABLE edu_dwb.act_student_leave_apply_detail partition (start_date)
select
-- 学生打卡记录表fact_tbh_student_signin_record
fsla.id  ,
fsla.class_id ,
fsla.student_id ,
fsla.audit_state ,
fsla.begin_time ,
fsla.begin_time_type,
fsla.end_time,
fsla.end_time_type  ,
fsla.cancel_state   ,
fsla.valid_state     ,
dcssc.id,
dcssc.class_id,
dcssc.studying_student_count,
dcssc.studying_date,
dctud.id,
dctud.class_id,
dctud.class_date,
dctud.content,
dtctt.id,
dtctt.class_id,
dtctt.morning_begin_time,
dtctt.morning_end_time,
dtctt.afternoon_begin_time,
dtctt.afternoon_end_time,
dtctt.evening_begin_time,
dtctt.evening_end_time,
dtctt.use_begin_date,
dtctt.use_end_date,
start_date
from edu_dwd.fact_student_leave_apply fsla
left join edu_dwd.dim_tbh_class_time_table dtctt
    on fsla.class_id = dtctt.class_id
left join edu_dwd.dim_course_table_upload_detail dctud
on  dctud.class_id = fsla.class_id
left join edu_dwd.dim_class_studying_student_count dcssc
on dcssc.class_id = dctud.class_id and dcssc.studying_date = dctud.class_date
where dctud.class_date >= substr(fsla.begin_time,1,10) and dctud.class_date <= substr(fsla.end_time,1,10);


--                    on fsla.time_table_id = dtctt.id
--          left join edu_dwd.dim_course_table_upload_detail dctud
--                    on ftssr.signin_date = dctud.class_date and dctud.class_id = dtctt.class_id
--          left join edu_dwd.dim_class_studying_student_count dcssc
--                    on dcssc.class_id = dctud.class_id and dcssc.studying_date = dctud.class_date
-- where dctud.class_date >= dtctt.use_begin_date
--   and dctud.class_date <= dtctt.use_end_date
--   and dctud.class_date is not null;