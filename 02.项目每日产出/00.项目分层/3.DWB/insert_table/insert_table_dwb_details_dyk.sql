-- 将数据插入到客户意向大宽表中
INSERT INTO edu_dwb.intention_details
SELECT
    -- 意向表
    fcr.create_date_time,
    year(fcr.create_date_time) AS yearinfo,
    month(fcr.create_date_time) AS monthinfo,
    day(fcr.create_date_time) AS dayinfo,
    hour(fcr.create_date_time) AS hourinfo,
    fcr.origin_type,
    CASE WHEN fcr.origin_type='NETSERVICE' THEN 0
           WHEN fcr.origin_type='PHONE' THEN 0
               ELSE 1 END AS origin_type_state,
    fcr.itcast_school_id,
    fcr.itcast_subject_id,
    fcr.customer_id,
-- 客户表
    dc.area,
--线索表
       fcc.clue_state,
    if(clue_state = 'VALID_NEW_CLUES',1,0)AS clue_state_stat,
-- 学科表
       CASE WHEN dis2.id =0 THEN -1
           WHEN dis2.id IS NULL THEN -1
           ELSE dis2.id END AS subject_id,
    dis2.name AS subject_name,
-- 校区表
    CASE WHEN dis.id =0 THEN -1
        WHEN dis.id IS NULL THEN -1
            ELSE dis.id END AS school_id,
    dis.name AS school_name,
--员工表
    tdepart_id,
-- 部门表
    dcd.name AS depart_name
FROM edu_dwd.fact_customer_relationship fcr --意向事实表
    LEFT JOIN edu_dwd.dim_employee de ON fcr.creator = de.id    --员工表
    LEFT JOIN edu_dwd.dim_scrm_department dcd ON de.tdepart_id = dcd.id --部门表
    LEFT JOIN edu_dwd.fact_customer_clue fcc ON fcr.id = fcc.customer_relationship_id --线索表
    LEFT JOIN edu_dwd.dim_itcast_school dis ON fcr.itcast_school_id = dis.id --校区表
    LEFT JOIN edu_dwd.dim_itcast_subject dis2 ON fcr.itcast_subject_id= dis2.id --学科表
    LEFT JOIN edu_dwd.dim_customer dc ON fcr.customer_id = dc.id    -- 客户表
-- 清洗内容
WHERE fcr.deleted = false;

INSERT INTO TABLE edu_dwb.clue_details
SELECT
       -- 线索表
        clue_state,
    if(clue_state = 'VALID_NEW_CLUES',1,0)AS clue_state_stat,
       fcc.create_date_time,
       year(fcc.create_date_time) AS yearinfo,
       month(fcc.create_date_time) AS monthinfo,
       day(fcc.create_date_time) AS dayinfo,
       hour(fcc.create_date_time) AS hourinfo,
       --意向表
       fcr.origin_type,
       CASE WHEN fcr.origin_type = 'NETSERVICE' THEN 0
           WHEN fcr.origin_type ='PHONE' THEN 0
               ELSE 1 END AS origin_type_state,
       -- 投诉表
       appeal_status
FROM edu_dwd.fact_customer_clue fcc --线索表
    LEFT JOIN edu_dwd.fact_customer_relationship fcr ON fcc.customer_relationship_id = fcr.id   --意向表
    LEFT JOIN edu_dwd.dim_customer_appeal dca ON dca.customer_relationship_first_id = fcr.id --投诉表
WHERE fcr.deleted = false
;