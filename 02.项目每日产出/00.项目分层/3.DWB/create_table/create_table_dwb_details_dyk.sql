-- 客户意向大宽表
CREATE TABLE IF NOT EXISTS edu_dwb.intention_details(
    -- 意向表customer_relationship(事实表)
create_date_time timestamp COMMENT '创建时间',
-- 根据创建时间转换
yearinfo int COMMENT '创建年',
monthinfo int COMMENT '创建月',
dayinfo int COMMENT '创建日',
hourinfo int COMMENT '创建小时',
--
origin_type string COMMENT'数据来源',
-- 根据数据来源转换
origin_type_state int COMMENT '0：线上，1：线下',
--
itcast_school_id int COMMENT '校区id',
itcast_subject_id int COMMENT '学科id',
customer_id int COMMENT '所属客户id',
    -- 客户表customer
area string COMMENT '所在区域',
    -- 线索表 customer_clue
clue_state string COMMENT '线索状态',
-- 根据线索状态转换
clue_state_stat int COMMENT'新老用户，0：老用户，1：新用户',
    -- 学科表 itcast_subject
subject_id string COMMENT '学科id',
subject_name string COMMENT '学科名称',
    -- 校区表 itcast_school
school_id int COMMENT '校区id',
school_name string COMMENT '校区名称',
    -- 员工表 employee
tdepart_id int COMMENT '直属部门',
    -- 部门表 scrm_department
depart_name string COMMENT '部门名称'
)comment '客户意向大宽表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

-- 线索量大宽表
CREATE TABLE IF NOT EXISTS edu_dwb.clue_details(
    -- 线索表customer_clue（事实表）
clue_state string COMMENT'线索状态',
clue_state_stat int COMMENT'新老用户，0：老用户，1：新用户',
create_date_time timestamp COMMENT '创建时间',
yearinfo int COMMENT '创建年',
monthinfo int COMMENT '创建月',
dayinfo int COMMENT '创建日',
hourinfo int COMMENT '创建小时',
    -- 意向表customer_relationship
origin_type string COMMENT'数据来源',
origin_type_state int COMMENT '0：线上，1：线下',
    -- 投诉表 customer_appeal
appeal_status int COMMENT '申诉状态'
)comment '客户线索大宽表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');