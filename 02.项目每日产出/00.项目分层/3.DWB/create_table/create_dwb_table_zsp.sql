--考勤出勤分析宽表表
create table edu_dwb.tbh_student_signin_record_detail
(
    id                    int comment '主键id',
    normal_class_flag     string comment '是否正课 1 正课 2 自习',
    time_table_id         int comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id              int comment '班级id',
    student_id            int comment '学员id',
    signin_time           string comment '签到时间',
    signin_date           string comment '签到日期',
    share_state           int comment '共享屏幕状态 0 否 1是  在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0 ',
    -- 从班级作息表关联得到的字段
    morning_template_id   int comment '上午出勤模板id',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id int comment '下午出勤模板id',
    afternoon_begin_time  string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time      string comment '晚上结束时间',
    use_begin_date        date comment '使用开始日期',
    use_end_date          date comment '使用结束日期',
-- 从班级排课表关联得到字段
    base_id               int comment '课程主表id',
    class_date            date comment '上课日期',
    content               string  comment '课程内容',
-- 在读学员人数表得到字段
    school_id              int     comment '校区id',
    studying_student_count int     comment '在读班级人数',
    studying_date          string  comment '在读日期'
)comment '考勤打卡信息表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

--请假出勤
create table if not exists edu_dwb.act_student_leave_apply_detail(
-- 学生请假表act_student_leave_apply
    leave_id      int comment '学生请假表id',
    class_id int comment '学生请假表班级id',
    leave_student_id int comment '学生请假表学生id',
    audit_state     string comment '审核状态 0 待审核 1 通过 2 不通过',
    begin_time      timestamp comment '请假开始时间',
    begin_time_type string comment '1：上午 2：下午',
    end_time        timestamp comment '请假结束时间',
    end_time_type   string comment '1：上午 2：下午',
    valid_state     string comment '是否有效（0：无效 1：有效）',
-- 班级在读学生人数 dim_class_studying_student_count
    count_id int comment '班级在读学生人数id',
    count_class_id int comment '班级在读学生人数班级id',
    studying_student_count int comment '在读班级人数',
    studying_date          date comment '在读日期',
-- 班级课表dim_course_table_upload_detail
    course_id int comment '班级课表id',
    course_class_id	int comment '班级课表班级id',
    class_date          date comment '上课日期',
    content             string comment '课程内容',
-- 班级休息时间表 dim_tbh_class_time_table
    time_id int comment '班级休息时间表id',
    time_class_id int comment '班级休息时间表班级id',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_begin_time  string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time      string comment '晚上结束时间',
    use_begin_date        date comment '使用开始日期',
    use_end_date          date comment '使用结束日期'
)comment '学生请假信息宽表'
partitioned by (dt  string comment '起始状态')
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');