
drop table if exists edu_dwb.student_detail;
CREATE TABLE IF NOT EXISTS edu_dwb.student_detail(
     id                    int comment '主键id',
    normal_class_flag     string comment '是否正课 ',
    time_table_id         int comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id              int comment '班级id',
    student_id            int comment '学员id',
    signin_time           timestamp comment '签到时间',
    signin_date           date comment '签到日期',
    share_state           string comment '共享屏幕状态   ',
    inner_ip          string comment '内网ip地址',

    base_id               int comment '课程主表id',
    class_date            date comment '上课日期',
    content      string comment '课程内容',

   morning_template_id   int comment '上午出勤模板id',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id int comment '下午出勤模板id',
    afternoon_begin_time   string comment '下午开始时间',
    afternoon_end_time     string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time     string comment '晚上开始时间',
    evening_end_time    string comment '晚上结束时间',
    use_begin_date      date comment '使用开始日期',
    use_end_date          date comment '使用结束日期',

    school_id              int     comment '校区id',
    studying_student_count int     comment '在读班级人数',
    studying_date         timestamp  comment '在读日期'
)comment '考勤打卡表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');


drop table if exists edu_dwb.leave_detail;
CREATE TABLE IF NOT EXISTS edu_dwb.leave_detail(
    class_id int comment '班级id',
    leave_student_id int comment '学生请假表学生id',
    audit_state     string comment '审核状态 ',
    audit_time      timestamp comment '审核时间',
    leave_type      string comment '请假类型  ',
    leave_reason    string comment '请假原因  ',
    begin_time      timestamp comment '请假开始时间',
    begin_time_type string comment '1：上午 2：下午',
    end_time        timestamp comment '请假结束时间',
    end_time_type   string comment '1：上午 2：下午',
    days            float comment '请假/已休天数',
    valid_state     string comment '是否有效',
    create_time     timestamp comment '创建时间',
    end_date        string,

   morning_template_id   int comment '上午出勤模板id',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id int comment '下午出勤模板id',
    afternoon_begin_time   string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time     string comment '晚上结束时间',
    use_begin_date        date comment '使用开始日期',
    use_end_date          date comment '使用结束日期',

    base_id               int comment '课程主表id',
    class_date            date comment '上课日期',
    content      string comment '课程内容',

    school_id              int     comment '校区id',
    studying_student_count int     comment '在读班级人数',
    studying_date          timestamp  comment '在读日期'
)comment '请假人数表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');


