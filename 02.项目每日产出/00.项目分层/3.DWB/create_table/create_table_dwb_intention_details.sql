USE edu_dwd;
/*
 建立宽表用来计算新老学员的意向用户个数
 指标：学员的意向用户个数
 维度：时间：每天/每月/每年
            线上/线下
            新/老学员
            地区      学科      校区      来源渠道        咨询中心
 */

CREATE TABLE IF NOT EXISTS edu_dwb.intention_details(
    --客户意向表字段customer_relationship
    id int  comment '客户意向id，唯一标记一条意向信息',
    create_date_time         timestamp  comment '数据创建时间',
    update_date_time  timestamp comment '最后更新时间',
    deleted timestamp COMMENT '判断字段，1表示已经被删，不用参加统计',
    customer_id   int comment '所属客户id,用来关联customer表中的id',
    first_id int comment '第一条客户关系id',
    belonger int comment '归属人',
    belonger_name string comment '归属人姓名',
    distribution_handler int comment '分配处理人',
    origin_type string comment '数据来源',
    origin_type_state int COMMENT '0代表数据来源为线下，1代表线上',
    creator int comment '意向数据创建人(负责这个意向信息的销售id，也就是咨询老师员工)',
    current_creator int comment '意向数据当前创建人：初始创建人，当在公海拉回时为 拉回人',
    creator_name string comment '创建者姓名',
    origin_channel  string comment '意向数据来源渠道',
    itcast_clazz_id int comment '所属ems班级id,对应报名课程表的id',
    itcast_clazz_time timestamp comment '报班时间',
    ems_student_id  int  comment 'ems的学生id',
    course_id   int comment '课程ID',
    course_name string comment '课程名称',
    rs_follow_type int  comment '意向数据分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取',
    transfer_bxg_oa_account  string comment '转移到博学谷归属人OA账号',
    -- 客户线索表字段customer_clue
    session_id string comment '七陌会话id',
    sid string comment '访客id',
    platform string comment '平台来源 （pc-网站咨询|wap-wap咨询|sdk-app咨询|weixin-微信咨询）',
    s_name string comment '用户名称',
    seo_source string comment '搜索来源',
    ip string comment 'IP地址',
    msg_count int comment '客户发送消息数',
    area string comment '区域',
    country string comment '所在国家',
    province string comment '省',
    city string comment '城市',
    valid int comment '该线索是否是网资有效线索',
    clue_state string comment '线索状态',
    clue_state_stat int COMMENT '新老用户，0代表老用户，1代表新用户VALID_NEW_CLUES为新用户，其他为老用户',
    tf_origin_channel string comment '投放渠道',
    zhuge_session_id  string comment '诸葛会话id',
    is_repeat int comment '是否重复线索(手机号维度) 0:正常 1：重复',
    tenant int comment '租户id',
    clue_follow_type int comment '线索数据分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取',
    -- 客户静态信息表customer
    use_name string comment '客户姓名',
    bxg_student_id string comment '博学谷学员ID，可能未关联到，不存在',
    -- 线索申诉信息表 customer_appeal
    customer_relationship_first_id int comment '第一条客户关系id',
    appeal_status int comment '申诉状态，0:待稽核 1:无效 2：有效',
    -- 员工信息表employee
    employee_id int comment '员工id  对应客户意向表的创建人id',
    real_name string comment '员工的真实姓名',
    tdepart_id int comment '直属部门  对应部门表的部门id',
    -- 员工部门表scrm_department
    department_name string comment '部门名称',
    -- 校区信息表itcast_school
    school_name string comment '校区名称',
    -- 学科信息表itcast_subject
    subject_name string comment '学科名称'
)comment '客户意向大宽表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

