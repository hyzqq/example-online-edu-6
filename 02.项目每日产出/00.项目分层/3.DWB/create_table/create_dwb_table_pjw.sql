create table if not exists edu_dwb.dwb_web_chat_ems_detail(
    -- 原来的字段
    id							   int	comment '主键',
    create_date_time        	   string comment '数据创建时间',
    session_id              	   string comment '会话系统id,计算会话数',
    sid                     	   string comment '访客id，计算访客数',
    create_time             	   string comment '会话创建时间',
    date_hour                      string comment '小时维度',
    seo_source              	   string comment '搜索来源',
    ip                      	   string comment 'IP地址，计算IP数',
    area                    	   string comment '地域',
    country                 	   string comment '所在国家',
    province                       string comment '省',
    city                           string comment '城市',
    origin_channel                 string comment '来源渠道(广告)',
    msg_count                      int comment '客户发送消息数',
    browser_name                   string comment '浏览器名称',
    os_info                        string comment '系统名称',
    -- 从副表关联得到的字段
    from_url             		   string comment'会话来源页面',
    -- 拉链表字段
    endTime						   string comment '拉链表状态终止时间'
)partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table edu_dwb.dwb_web_chat_ems_detail;