--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;


--学生打卡表
insert overwrite table edu_dwd.fact_tbh_student_signin_record partition (dt)
select id,
       case normal_class_flag
           when 1 then '正课'
           when 2 then '自习'
           else 'other' end          normal_class_flag,
       time_table_id,
       class_id,
       student_id,
       signin_time,
       signin_date,
       share_state,
       '9999-99-99'                  end_date,
       dt
from edu_ods.tbh_student_signin_record;
--学生请假表
insert overwrite table edu_dwd.fact_student_leave_apply partition (dt)
select id,
       class_id,
       student_id,
       case audit_state
           when 0 then '待审核'
           when 1 then '通过'
           when 2 then '不通过'
           else 'other' end       as audit_state,
       begin_time,
       case begin_time_type
           when 1 then '上午'
           when 2 then '下午'
           else 'other' end       as begin_time_type,
       end_time,
       case end_time_type
           when 1 then '上午'
           when 2 then '下午'
           else 'other' end       as end_time_type,
       days,
       valid_state,
       create_time,
       '9999-99-99'               as end_date,
        dt
from edu_ods.student_leave_apply;

