create table if not exists edu_dwd.dwd_fact_web_chat_ems(
    id							   int comment '主键',
    create_date_time        	   timestamp comment '数据创建时间',
    session_id              	   string comment '会话系统id,计算会话数',
    sid                     	   string comment '访客id，计算访客数',
    create_time             	   timestamp comment '会话创建时间',
    seo_source              	   string comment '搜索来源',
    ip                      	   string comment 'IP地址，计算IP数',
    area                    	   string comment '地域',
    country                 	   string comment '所在国家',
    province                       string comment '省',
    city                           string comment '城市',
    origin_channel                 string comment '来源渠道(广告)',
    msg_count                      int comment '客户发送消息数',
    browser_name                   string comment '浏览器名称',
    os_info                        string comment '系统名称',
    from_url             		   string comment '会话来源页面'
)partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');