--学生打卡信息表 (ft)
create table edu_dwd.fact_tbh_student_signin_record
(
    id                int           comment '主键id',
    normal_class_flag string           comment '是否正课 1 正课 2 自习',
    time_table_id     int           comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id          int           comment '班级id',
    student_id        int           comment '学员id',
    signin_time       string        comment '签到时间',
    signin_date       string        comment '签到日期',
    share_state       int           comment '共享屏幕状态 0 否 1是  在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0 ',
    end_date          string        comment '拉链表结束日期'
)comment '学生打卡信息表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
--学生请假申请表（ft）
create table edu_dwd.fact_student_leave_apply
(
  id              int ,
  class_id        int             comment '班级id',
  student_id      int             comment '学员id',
  audit_state     string             comment '审核状态 0 待审核 1 通过 2 不通过',
  begin_time      string          comment '请假开始时间',
  begin_time_type string             comment '1：上午 2：下午',
  end_time        string          comment '请假结束时间',
  end_time_type   string             comment '1：上午 2：下午',
  days            decimal(38,2)   comment '请假/已休天数',
  valid_state     string             comment '是否有效（0：无效 1：有效）',
  create_time     string          comment '创建时间',
  end_date          string        comment '拉链表结束日期'
)comment '学生请假申请表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
