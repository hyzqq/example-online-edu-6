insert overwrite table   edu_dwd.fact_tbh_student_signin_record  partition(dt)
select
id,
case normal_class_flag
when 1 then '正课'
when 2 then '自习'
else 'others'
end as normal_class_flag,
time_table_id,
class_id,
student_id,
signin_time,
signin_date,
case inner_flag
when 0 then '外网'
when 1 then '内网'
else 'others'
end as inner_flag,
case signin_type
when 1 then '心跳打卡'
when 2 then '老师补卡'
else 'others'
end as signin_type,
case share_state
when 0 then '否'
when 1 then '是'
else 'others'
end as share_state,
inner_ip,
dt as start_date,
'9999-99-99' as end_date
from edu_ods.ods_tbh_student_signin_record;

insert overwrite table edu_dwd.fact_student_leave_apply partition(dt)
select
id,
class_id,
student_id,
case audit_state
when 0 then '待审核'
when 1 then '通过'
when 2 then '不通过'
else 'others'
end as audit_state,
audit_person,
audit_time,
audit_remark,
case leave_type
when 1 then '请假'
when 2 then '销假'
else 'others'
end as leave_type,
case leave_reason
when 1 then '事假'
when 2 then '病假'
else 'others'
end as leave_reason,
begin_time,
case begin_time_type
when   1 then '上午'
when 2 then '下午'
else 'others'
end as begin_time_type,
end_time,
case end_time_type
when   1 then '上午'
when 2 then '下午'
else 'others'
end as end_time_type,
days,
case cancel_state
when 0 then '未撤销'
when 1 then '已撤销'
else 'others'
end as  cancel_state,
cancel_time,
old_leave_id,
leave_remark,
case valid_state
when 0 then '无效'
when 1 then '有效'
else 'others'
end as valid_state,
create_time,
dt as start_date,
'9999-99-99' as end_date
from edu_ods.ods_student_leave_apply;

