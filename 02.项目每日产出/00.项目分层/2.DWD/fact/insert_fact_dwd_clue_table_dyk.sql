--设置动态分区的模式为  nonstrict非严格模式
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

--todo 将数据插入到客户意向事实表fact_customer_relationship
INSERT INTO edu_dwd.fact_customer_relationship
SELECT id,
       create_date_time,
       Year(create_date_time) AS create_year,
       Month(create_date_time) AS create_month,
       day(create_date_time) AS create_day,
       update_date_time,
       deleted,
       customer_id,
       first_id,
       belonger,
       belonger_name,
       initial_belonger,
       distribution_handler,
       business_scrm_department_id,
       last_visit_time,
       next_visit_time,
       origin_type,
       CASE WHEN origin_type ='NETSERVICE' THEN 1
                            ELSE 0 END AS origin_type_state,
       itcast_school_id,
       itcast_subject_id,
       intention_study_type,
       anticipat_signup_date,
       level,
       creator,
       current_creator,
       creator_name,
       origin_channel,
       comment,
       first_customer_clue_id,
       last_customer_clue_id,
       process_state,
       process_time,
       payment_state,
       payment_time,
       signup_state,
       signup_time,
       notice_state,
       notice_time,
       lock_state,
       lock_time,
       itcast_clazz_id,
       itcast_clazz_time,
       payment_url,
       payment_url_time,
       ems_student_id,
       delete_reason,
       deleter,
       deleter_name,
       delete_time,
       course_id,
       course_name,
       delete_comment,
       close_state,
       close_time,
       appeal_id,
       tenant,
       total_fee,
       belonged,
       belonged_time,
       belonger_time,
       transfer,
       transfer_time,
       follow_type,
       transfer_bxg_oa_account,
       transfer_bxg_belonger_name
FROM edu_ods.customer_relationship;
--todo 将数据插入到线索申诉信息事实表fact_customer_appeal
INSERT INTO edu_dwd.fact_customer_appeal
SELECT id,
       customer_relationship_first_id,
       employee_id,
       employee_name,
       employee_department_id,
       employee_tdepart_id,
       appeal_status,
       audit_id,
       audit_name,
       audit_department_id,
       audit_department_name,
       audit_date_time,
       create_date_time,
       update_date_time,
       deleted,
       tenant
FROM edu_ods.customer_appeal;