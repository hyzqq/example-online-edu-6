insert overwrite table edu_dwd.dwd_fact_customer_relationship
select id,
       create_date_time,
       update_date_time,
       deleted,
       customer_id,
       first_id,
       belonger,
       belonger_name,
       initial_belonger,
       distribution_handler,
       business_scrm_department_id,
       last_visit_time,
       next_visit_time,
       origin_type,
       intention_study_type,
       anticipat_signup_date,
       level,
       creator,
       current_creator,
       creator_name,
       origin_channel,
       comment,
       first_customer_clue_id,
       last_customer_clue_id,
       process_state,
       process_time,
       payment_state,
       payment_time,
       signup_state,
       signup_time,
       notice_state,
       notice_time,
       lock_state,
       lock_time,
       itcast_clazz_id,
       itcast_clazz_time,
       payment_url,
       payment_url_time,
       ems_student_id,
       delete_reason,
       deleter,
       deleter_name,
       delete_time,
       course_id,
       course_name,
       delete_comment,
       close_state,
       close_time,
       appeal_id,
       tenant,
       total_fee,
       belonged,
       belonged_time,
       belonger_time,
       transfer,
       transfer_time,
       follow_type,
       transfer_bxg_oa_account,
       transfer_bxg_belonger_name,
       if(itcast_school_id is NULL ,-1,itcast_school_id) as itcast_school_id,
       if(itcast_subject_id is NULL ,-1,itcast_subject_id) as itcast_subject_id,
       substr(create_date_time,1,4) as year,
       substr(create_date_time,1,7) as month,
       substr(create_date_time,1,10) as day,
       case  when origin_type='NETSERVICE' or origin_type='PHONE' then 1
when origin_type='OTHER' or origin_type='SCHOOL' or origin_type='VISITED' then 0
else -1
end as  origin_type_state
from edu_ods.ods_customer_relationship
where deleted=0;--数据是否被禁用，为1被禁用，为0没有被禁用
-- and itcast_subject_id<>0
-- and itcast_subject_id is not null--判断学科是否为存在学科
-- and itcast_school_id<>0
-- and itcast_school_id is not null--判断学校是否存在


insert overwrite table edu_dwd.dwd_fact_customer_clue
select *
from edu_ods.ods_customer_clue
where deleted=0; --数据是否被禁用，为1被禁用，为0没有被禁用
--and valid=0--线索是否为有效线索，1为无效，0为有效
