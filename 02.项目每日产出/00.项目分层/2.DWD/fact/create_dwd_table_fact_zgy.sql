--事实表
CREATE TABLE edu_dwd.fact_tbh_student_signin_record
(
    id           INT  COMMENT '主键id',
    normal_class_flag string         COMMENT '是否正课 1 正课 2 自习',
    time_table_id     INT      COMMENT '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id          INT         COMMENT '班级id',
    student_id        INT       COMMENT '学员id',
    signin_time       timestamp     COMMENT '签到时间',
    signin_date     date        COMMENT '签到日期',
    inner_flag        string       COMMENT '内外网标志  0 外网 1 内网',
    signin_type      string         COMMENT '签到类型 1 心跳打卡 2 老师补卡',
    share_state       string   COMMENT '共享屏幕状态 0 否 1是  在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0 ',
    inner_ip         string   COMMENT '内网ip地址',
    end_date  string
)comment '学员打卡信息表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

CREATE TABLE edu_dwd.fact_student_leave_apply
(
    id              INT,
    class_id        INT            COMMENT '班级id',
    student_id      INT          COMMENT '学员id',
    audit_state     string COMMENT '审核状态 0 待审核 1 通过 2 不通过',
    audit_person    INT      COMMENT '审核人',
    audit_time      timestamp     COMMENT '审核时间',
    audit_remark    string COMMENT '审核备注',
    leave_type      string       COMMENT '请假类型  1 请假 2 销假',
    leave_reason   string       COMMENT '请假原因  1 事假 2 病假',
    begin_time      timestamp   COMMENT '请假开始时间',
    begin_time_type string      COMMENT '1：上午 2：下午',
    end_time        timestamp   COMMENT '请假结束时间',
    end_time_type  string        COMMENT '1：上午 2：下午',
    days            FLOAT        COMMENT '请假/已休天数',
    cancel_state    string  COMMENT '撤销状态  0 未撤销 1 已撤销',
    cancel_time     timestamp   COMMENT '撤销时间',
    old_leave_id    INT     COMMENT '原请假id，只有leave_type =2 销假的时候才有',
    leave_remark    string  COMMENT '请假/销假说明',
    valid_state    string  COMMENT '是否有效（0：无效 1：有效）',
    create_time     timestamp    COMMENT '创建时间',
    end_date string
)comment '学生请假申请表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');
--------------------------------------------------------------------------------
