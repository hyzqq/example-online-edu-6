-- 创建事实表
-- 创建edu.dwd层学生请假申请表
drop table edu_dwd.fact_student_leave_apply;
CREATE TABLE edu_dwd.fact_student_leave_apply
(
    id INT COMMENT '主键id',
    class_id INT COMMENT '班级id',
    student_id INT COMMENT '学员id',
    audit_state string  COMMENT '审核状态 0 待审核 1 通过 2 不通过',
    audit_person INT COMMENT '审核人',
    audit_time TIMESTAMP COMMENT '审核时间',  -- DATETIME 转为 TIMESTAMP
    audit_remark STRING COMMENT '审核备注',  -- VARCHAR 转为 STRING
    leave_type string COMMENT '请假类型  1 请假 2 销假',
    leave_reason string COMMENT '请假原因  1 事假 2 病假',
    begin_time TIMESTAMP COMMENT '请假开始时间',  -- DATETIME 转为 TIMESTAMP
    begin_time_type string COMMENT '1：上午 2：下午',
    end_time TIMESTAMP COMMENT '请假结束时间',  -- DATETIME 转为 TIMESTAMP
    end_time_type string COMMENT '1：上午 2：下午',
    days FLOAT COMMENT '请假/已休天数',
    cancel_state string  COMMENT '撤销状态  0 未撤销 1 已撤销',
    cancel_time TIMESTAMP COMMENT '撤销时间',  -- DATETIME 转为 TIMESTAMP
    old_leave_id INT COMMENT '原请假id，只有leave_type =2 销假的时候才有',
    leave_remark STRING COMMENT '请假/销假说明',  -- VARCHAR 转为 STRING
    valid_state string  COMMENT '是否有效（0：无效 1：有效）',
    create_time TIMESTAMP COMMENT '创建时间',  -- DATETIME 转为 TIMESTAMP
    end_date string comment '末状态'
)comment '学生请假申请表'
partitioned by(start_date string) -- 单独看 表示是分区  和end_time一起看 表示的是拉链起始时间
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩

-- 创建创建dwd层学生打卡信息表--拉链表
drop table if exists edu_dwd.fact_tbh_student_signin_record;
CREATE TABLE edu_dwd.fact_tbh_student_signin_record
(
    id INT COMMENT '主键id',
    normal_class_flag string COMMENT '是否正课 1 正课 2 自习',
    time_table_id int COMMENT '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id INT COMMENT '班级id',
    student_id INT COMMENT '学员id',
    signin_time TIMESTAMP COMMENT '签到时间', -- DATETIME 在 Hive 中用 TIMESTAMP 类型表示
    signin_date DATE COMMENT '签到日期',
    inner_flag string COMMENT '内外网标志 0 外网 1 内网',
    signin_type string COMMENT '签到类型 1 心跳打卡 2 老师补卡',
    share_state string COMMENT '共享屏幕状态 0 否 1是 在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0',
    inner_ip STRING COMMENT '内网ip地址', -- VARCHAR 在 Hive 中使用 STRING 类型
    end_date string comment '末状态'
)
COMMENT '学生打卡信息表'
partitioned by(start_date string) -- 单独看 表示是分区  和end_time一起看 表示的是拉链起始时间
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩