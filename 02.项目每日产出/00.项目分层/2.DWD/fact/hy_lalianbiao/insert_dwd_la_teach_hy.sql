-- 设置动态分区参数
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
-- 首先取出昨天导入的数据作为初始拉链表
insert into table edu_dwd.fact_tbh_student_signin_record partition (start_date)
SELECT id,
       CASE normal_class_flag
           WHEN 1 THEN '正课'
           WHEN 2 THEN '自习'
           ELSE '未定义'
           END      AS normal_class_flag,
       time_table_id,
       class_id,
       student_id,
       signin_time,
       signin_date,
       CASE inner_flag
           WHEN 0 THEN '外网'
           WHEN 1 THEN '内网'
           ELSE '未定义'
           END,
       CASE signin_type
           WHEN 1 THEN '心跳打卡'
           WHEN 2 THEN '老师补卡'
           ELSE '未定义'
           END,
       CASE share_state
           WHEN 0 THEN '否'
           WHEN 1 THEN '是'
           ELSE '未定义'
           END,
       inner_ip,
       '9999-99-99' as end_date,
       export_date  as start_date
FROM edu_ods.tbh_student_signin_record
where export_date = date_sub(current_date, 2);
insert into table edu_dwd.fact_student_leave_apply partition (start_date)
SELECT id,
       class_id,
       student_id,
       CASE audit_state
           WHEN 0 THEN '待审核'
           WHEN 1 THEN '通过'
           WHEN 2 THEN '不通过'
           ELSE '状态未知'
           END      AS audit_state,
       audit_person,
       audit_time,
       audit_remark,
       CASE leave_type
           WHEN 1 THEN '请假'
           WHEN 2 THEN '销假'
           ELSE '类型未知'
           END      AS leave_type,
       CASE leave_reason
           WHEN 1 THEN '事假'
           WHEN 2 THEN '病假'
           ELSE '原因未知'
           END      AS leave_reason,
       begin_time,
       CASE begin_time_type
           WHEN 1 THEN '上午'
           WHEN 2 THEN '下午'
           ELSE '时间段未知'
           END      AS begin_time_type,
       end_time,
       CASE end_time_type
           WHEN 1 THEN '上午'
           WHEN 2 THEN '下午'
           ELSE '时间段未知'
           END      AS end_time_type,
       days,
       CASE cancel_state
           WHEN 0 THEN '未撤销'
           WHEN 1 THEN '已撤销'
           ELSE '撤销状态未知'
           END      AS cancel_state,
       cancel_time,
       old_leave_id,
       leave_remark,
       CASE valid_state
           WHEN 0 THEN '无效'
           WHEN 1 THEN '有效'
           ELSE '有效性未知'
           END      AS valid_state,
       create_time,
       '9999-99-99' as end_date,
       export_date  as start_date
FROM edu_ods.student_leave_apply;
-- 首先确定里面存在新增的数据即export_date为今天2024-05-04---这里没有更新的数据（在mysql更新，再使用sqoop导入太慢了，就不更新了）

-- todo step1.0 将初始拉链表与增量表进行left join,因为mysql中id为主键所以如果是更新的话那么id必然不变，否则就是新增
-- todo step1.1 首先筛选出增量数据分析前一天的就改为date_sub(current_date,1)

with insert_table as (select id,
                             normal_class_flag,
                             time_table_id,
                             class_id,
                             student_id,
                             signin_time,
                             signin_date,
                             inner_flag,
                             signin_type,
                             share_state,
                             inner_ip,
                             export_date
                      from edu_ods.tbh_student_signin_record
                      where export_date = date_sub(current_date, 1)),
-- todo step1.2 将初始拉链表与增量表进行左连接，判断数据是否被修改，如果是将末状态(结束时间)改为export_date的前一天,判断条件为增量表的id不为null
     temp_lalian1 as (select dwd_sigin.id,
                             dwd_sigin.normal_class_flag,
                             dwd_sigin.time_table_id,
                             dwd_sigin.class_id,
                             dwd_sigin.student_id,
                             dwd_sigin.signin_time,
                             dwd_sigin.signin_date,
                             dwd_sigin.inner_flag,
                             dwd_sigin.signin_type,
                             dwd_sigin.share_state,
                             dwd_sigin.inner_ip,
                             if(insert_table.id is not null, date_sub(export_date, 1), end_date) as end_date,
                             start_date
                      from edu_dwd.fact_tbh_student_signin_record dwd_sigin
                               left join insert_table on dwd_sigin.id = insert_table.id),
-- todo  step2.1 将上面的来结果作为结果集A与增量表进行union all
     temp__lalian2 as (select id,
                              normal_class_flag,
                              time_table_id,
                              class_id,
                              student_id,
                              signin_time,
                              signin_date,
                              inner_flag,
                              signin_type,
                              share_state,
                              inner_ip,
                              end_date,
                              start_date
                       from temp_lalian1
                       union all
                       select id,
                              CASE normal_class_flag
                                  WHEN 1 THEN '正课'
                                  WHEN 2 THEN '自习'
                                  ELSE '未定义'
                                  END AS normal_class_flag,
                              time_table_id,
                              class_id,
                              student_id,
                              signin_time,
                              signin_date,
                              CASE inner_flag
                                  WHEN 0 THEN '外网'
                                  WHEN 1 THEN '内网'
                                  ELSE '未定义'
                                  END,
                              CASE signin_type
                                  WHEN 1 THEN '心跳打卡'
                                  WHEN 2 THEN '老师补卡'
                                  ELSE '未定义'
                                  END,
                              CASE share_state
                                  WHEN 0 THEN '否'
                                  WHEN 1 THEN '是'
                                  ELSE '未定义'
                                  END,
                              inner_ip,
                              '9999-99-99',
                              export_date
                       from edu_ods.tbh_student_signin_record
                       where export_date = date_sub(current_date, 1))

insert  overwrite table edu_dwd.fact_tbh_student_signin_record partition (start_date)
select *
from temp__lalian2;
