--todo customer_appeal线索申诉信息事实表
CREATE TABLE IF NOT EXISTS fact_customer_appeal (
  id INT COMMENT '主键',
  customer_relationship_first_id INT COMMENT '第一条客户关系id',
  employee_id INT COMMENT '申诉人',
  employee_name STRING COMMENT '申诉人姓名',
  employee_department_id INT COMMENT '申诉人部门',
  employee_tdepart_id INT COMMENT '申诉人所属部门',
  appeal_status INT COMMENT '申诉状态，0:待稽核 1:无效 2：有效',
  audit_id INT COMMENT '稽核人id',
  audit_name STRING COMMENT '稽核人姓名',
  audit_department_id INT COMMENT '稽核人所在部门',
  audit_department_name STRING COMMENT '稽核人部门名称',
  audit_date_time TIMESTAMP COMMENT '稽核时间',
  create_date_time TIMESTAMP COMMENT '创建时间（申诉时间），默认值需在数据加载时指定',
  update_date_time TIMESTAMP COMMENT '更新时间，默认为数据加载时的时间并可随数据更新而更新，需在数据处理流程中实现',
  deleted BOOLEAN COMMENT '删除标志位，默认为false',
  tenant INT COMMENT '租户ID，默认为0'
)
COMMENT '线索申诉信息事实表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--todo fact_customer_relationship客户意向事实表
CREATE TABLE IF NOT EXISTS fact_customer_relationship (
  id INT COMMENT '自增ID，需在数据导入时自动生成',
  create_date_time TIMESTAMP COMMENT '创建时间',
  --下面这部分由create_date_time提取
  create_year timestamp COMMENT '创建年',
  create_month timestamp COMMENT '创建月',
  create_day timestamp COMMENT '创建日',
    --
  update_date_time TIMESTAMP COMMENT '最后更新时间，需在数据更新时自行维护',
  deleted BOOLEAN COMMENT '是否被删除（禁用），默认为false',
  customer_id INT COMMENT '所属客户id',
  first_id INT COMMENT '第一条客户关系id',
  belonger INT COMMENT '归属人',
  belonger_name STRING COMMENT '归属人姓名',
  initial_belonger INT COMMENT '初始归属人',
  distribution_handler INT COMMENT '分配处理人',
  business_scrm_department_id INT COMMENT '归属部门，默认为0',
  last_visit_time TIMESTAMP COMMENT '最后回访时间',
  next_visit_time TIMESTAMP COMMENT '下次回访时间',
  origin_type STRING COMMENT '数据来源',
  -- 下面这部分由origin_type转换
  origin_type_state int COMMENT '0代表数据来源为线下，1代表线上',
  --
  itcast_school_id INT COMMENT '校区Id',
  itcast_subject_id INT COMMENT '学科Id',
  intention_study_type STRING COMMENT '意向学习方式',
  anticipat_signup_date DATE COMMENT '预计报名时间',
  level STRING COMMENT '客户级别',
  creator INT COMMENT '创建人',
  current_creator INT COMMENT '当前创建人：初始==创建人，当在公海拉回时为 拉回人',
  creator_name STRING COMMENT '创建者姓名',
  origin_channel STRING COMMENT '来源渠道',
  comment STRING COMMENT '备注',
  first_customer_clue_id INT COMMENT '第一条线索id，默认为0',
  last_customer_clue_id INT COMMENT '最后一条线索id，默认为0',
  process_state STRING COMMENT '处理状态',
  process_time TIMESTAMP COMMENT '处理状态变动时间',
  payment_state STRING COMMENT '支付状态',
  payment_time TIMESTAMP COMMENT '支付状态变动时间',
  signup_state STRING COMMENT '报名状态',
  signup_time TIMESTAMP COMMENT '报名时间',
  notice_state STRING COMMENT '通知状态',
  notice_time TIMESTAMP COMMENT '通知状态变动时间',
  lock_state BOOLEAN COMMENT '锁定状态，默认为false',
  lock_time TIMESTAMP COMMENT '锁定状态修改时间',
  itcast_clazz_id INT COMMENT '所属ems班级id',
  itcast_clazz_time TIMESTAMP COMMENT '报班时间',
  payment_url STRING COMMENT '付款链接',
  payment_url_time TIMESTAMP COMMENT '支付链接生成时间',
  ems_student_id INT COMMENT 'ems的学生id',
  delete_reason STRING COMMENT '删除原因',
  deleter INT COMMENT '删除人',
  deleter_name STRING COMMENT '删除人姓名',
  delete_time TIMESTAMP COMMENT '删除时间',
  course_id INT COMMENT '课程ID',
  course_name STRING COMMENT '课程名称',
  delete_comment STRING COMMENT '删除原因说明',
  close_state STRING COMMENT '关闭状态',
  close_time TIMESTAMP COMMENT '关闭状态变动时间',
  appeal_id INT COMMENT '申诉id',
  tenant INT COMMENT '租户，默认为0',
  total_fee DECIMAL(19,0) COMMENT '报名费总金额',
  belonged INT COMMENT '小周期归属人',
  belonged_time TIMESTAMP COMMENT '归属时间',
  belonger_time TIMESTAMP COMMENT '归属时间',
  transfer INT COMMENT '转移人',
  transfer_time TIMESTAMP COMMENT '转移时间',
  follow_type INT COMMENT '分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取，默认为0',
  transfer_bxg_oa_account STRING COMMENT '转移到博学谷归属人OA账号',
  transfer_bxg_belonger_name STRING COMMENT '转移到博学谷归属人OA姓名'
)
COMMENT '客户意向事实表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');