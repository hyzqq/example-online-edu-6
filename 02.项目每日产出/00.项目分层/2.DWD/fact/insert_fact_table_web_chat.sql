insert overwrite table edu_dwd.dwd_fact_web_chat_ems partition(dt)
select a.id,
       a.create_date_time,
       a.session_id,
       a.sid,
       a.create_time,
       a.seo_source,
       a.ip,
       a.area,
       a.country,
       a.province,
       a.city,
       a.origin_channel,
       a.msg_count,
       a.browser_name,
       a.os_info,
       b.from_url,
       substr(a.create_time,1,10) as dt
from edu_ods.ods_web_chat_ems_2019_07 a
         left join edu_ods.ods_web_chat_text_ems_2019_07 b on a.id = b.id
where sid is not null;