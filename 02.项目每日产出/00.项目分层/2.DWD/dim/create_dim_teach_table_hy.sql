-- 创建维度表

-- 创建dwd层在读学员人数信息表
drop table if exists edu_dwd.dim_class_studying_student_count;
CREATE TABLE edu_dwd.dim_class_studying_student_count
(
    id INT,
    school_id INT COMMENT '校区id',
    subject_id INT COMMENT '学科id',
    class_id INT COMMENT '班级id',
    studying_student_count INT COMMENT '在读班级人数',
    studying_date DATE  COMMENT '在读日期'
)comment '在读学员人数信息表'
partitioned by (export_date string)
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定ORC格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用Snappy压缩


--创建edu.dwd层班级作息时间表
drop table if exists edu_dwd.dim_tbh_class_time_table;
CREATE TABLE edu_dwd.dim_tbh_class_time_table
(
    id INT COMMENT '主键id',
    class_id INT COMMENT '班级id',
    morning_template_id INT COMMENT '上午出勤模板id',
    morning_begin_time STRING COMMENT '上午开始时间',  -- Hive中TIME类型通常使用STRING表示
    morning_end_time STRING COMMENT '上午结束时间',
    afternoon_template_id INT COMMENT '下午出勤模板id',
    afternoon_begin_time STRING COMMENT '下午开始时间',
    afternoon_end_time STRING COMMENT '下午结束时间',
    evening_template_id INT COMMENT '晚上出勤模板id',
    evening_begin_time STRING COMMENT '晚上开始时间',
    evening_end_time STRING COMMENT '晚上结束时间',
    use_begin_date DATE COMMENT '使用开始日期',
    use_end_date DATE COMMENT '使用结束日期',
    create_time TIMESTAMP COMMENT '创建时间',  -- DATETIME 在 Hive 中用 TIMESTAMP 表示
    create_person INT COMMENT '创建人',
    remark STRING COMMENT '备注'  -- VARCHAR(500) 在 Hive 中使用 STRING 类型
)comment '班级作息时间表'
PARTITIONED BY (export_date STRING) -- 添加分区字段
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩

-- 创建edu.dwd层班级排课信息表
drop table if exists edu_dwd.dim_course_table_upload_detail;
CREATE TABLE edu_dwd.dim_course_table_upload_detail
(
    id INT COMMENT 'id',  -- 去除 AUTO_INCREMENT，假设在数据加载时已保证其唯一性
    base_id INT COMMENT '课程主表id',
    class_id INT COMMENT '班级id',
    class_date DATE COMMENT '上课日期',
    content STRING COMMENT '课程内容',  -- VARCHAR 转为 STRING
    teacher_id INT COMMENT '老师id',
    teacher_name STRING COMMENT '老师名字',  -- VARCHAR 转为 STRING
    job_number STRING COMMENT '工号',  -- VARCHAR 转为 STRING
    classroom_id INT COMMENT '教室id',
    classroom_name STRING COMMENT '教室名称',  -- VARCHAR 转为 STRING
    is_outline INT COMMENT '是否大纲 0 否 1 是',
    class_mode INT COMMENT '上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播',
    is_stage_exam INT COMMENT '是否阶段考试（0：否 1：是）',
    is_pay INT COMMENT '代课费（0：无 1：有）',
    tutor_teacher_id INT COMMENT '晚自习辅导老师id',
    tutor_teacher_name STRING COMMENT '辅导老师姓名',  -- VARCHAR 转为 STRING
    tutor_job_number STRING COMMENT '晚自习辅导老师工号',  -- VARCHAR 转为 STRING
    is_subsidy INT COMMENT '晚自习补贴（0：无 1：有）',
    answer_teacher_id INT COMMENT '答疑老师id',
    answer_teacher_name STRING COMMENT '答疑老师姓名',  -- VARCHAR 转为 STRING
    answer_job_number STRING COMMENT '答疑老师工号',  -- VARCHAR 转为 STRING
    remark STRING COMMENT '备注',  -- VARCHAR 转为 STRING
    create_time TIMESTAMP COMMENT '创建时间'  -- DATETIME 转为 TIMESTAMP
)comment '班级排课信息表'
PARTITIONED BY (export_date STRING) -- 添加分区字段
row format delimited
fields terminated by '/t'
STORED AS ORC -- 指定 ORC 格式存储
TBLPROPERTIES ("orc.compress" = "SNAPPY"); -- 使用 Snappy 压缩

