--班级作息时间表(dt)
create table edu_dwd.dim_tbh_class_time_table
(
    id                    int          comment '主键id',
    class_id              int          comment '班级id',
    morning_template_id   int          comment '上午出勤模板id',
    morning_begin_time    string       comment '上午开始时间',
    morning_end_time      string       comment '上午结束时间',
    afternoon_template_id int          comment '下午出勤模板id',
    afternoon_begin_time  string       comment '下午开始时间',
    afternoon_end_time    string       comment '下午结束时间',
    evening_template_id   int          comment '晚上出勤模板id',
    evening_begin_time    string       comment '晚上开始时间',
    evening_end_time      string       comment '晚上结束时间',
    use_begin_date        date comment '使用开始日期',
    use_end_date          date comment '使用结束日期',
    create_time          string       comment '创建时间'
)comment '班级作息时间表'

row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
--班级排课信息表（dt）
create table edu_dwd.dim_course_table_upload_detail
(
    id                  int           comment 'id',
    base_id             int           comment '课程主表id',
    class_id            int           comment '班级id',
    class_date          string        comment '上课日期',
    content              string        comment '课程内容',
    create_time         string        comment '创建时间'
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
--在读学员人数信息表
create table edu_dwd.dim_class_studying_student_count
(
    id                     int ,
    school_id              int     comment '校区id',
    subject_id             int     comment '学科id',
    class_id               int     comment '班级id',
    studying_student_count int     comment '在读班级人数',
    studying_date          date  comment '在读日期'
)
row format delimited fields terminated by '\t'
stored as orc
  tblproperties ('orc.compress' = 'SNAPPY');