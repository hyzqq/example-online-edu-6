insert overwrite table edu_dwd.dim_class_studying_student_count partition(dt)
select
id,
school_id,
subject_id,
class_id,
studying_student_count,
studying_date,
dt as  start_date,
'9999-99-99' as end_date
from edu_ods.ods_class_studying_student_count;


insert overwrite table edu_dwd.dim_course_table_upload_detail partition (dt)
select
id,
base_id,
class_id,
class_date,
content,
teacher_id,
teacher_name,
job_number,
classroom_id,
classroom_name,
is_outline,
class_mode,
is_stage_exam,
is_pay,
tutor_teacher_id,
tutor_teacher_name,
tutor_job_number,
is_subsidy,
answer_teacher_id,
answer_teacher_name,
answer_job_number,
remark,
create_time,
dt as start_date,
'9999-99-99' as end_date
from edu_ods.ods_course_table_upload_detail;


insert overwrite table edu_dwd.dim_tbh_class_time_table partition (dt)
select id,
class_id,
morning_template_id,
morning_begin_time,
morning_end_time,
afternoon_template_id,
afternoon_begin_time,
afternoon_end_time,
evening_template_id,
evening_begin_time,
evening_end_time,
use_begin_date,
use_end_date,
create_time,
create_person,
remark,
dt as start_date,
'9999-99-99' as end_date
from edu_ods.ods_tbh_class_time_table;