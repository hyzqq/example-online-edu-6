create table if not exists edu_dwd.dwd_dim_area(
    id			int	comment '主键' ,
    area        string comment '地域',
    country     string comment '所在国家',
    province    string comment '省',
    city        string comment '城市'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

create table if not exists edu_dwd.dwd_dim_date(
    id						int	comment '主键',
    create_date_time        timestamp comment '数据创建时间',
    create_time             timestamp comment '会话创建时间',
    date_hour               string comment '小时维度'
)row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');