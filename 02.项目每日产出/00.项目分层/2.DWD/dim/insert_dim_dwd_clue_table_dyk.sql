--设置动态分区的模式为  nonstrict非严格模式
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

--todo 插入数据到dim_custmoer  客户静态信息维度表
INSERT INTO edu_dwd.dim_customer
SELECT id,
       customer_relationship_id,
       create_date_time,
       update_date_time,
       deleted,
       name,
       idcard,
       birth_year,
       gender,
       phone,
       wechat,
       qq,
       email,
       area,
       leave_school_date,
       graduation_date,
       bxg_student_id,
       creator,
       origin_type,
       origin_channel,
       tenant,
       md_id
FROM edu_ods.customer;

--todo 插入数据到dim_customer_clue客户线索维度表
INSERT INTO edu_dwd.dim_customer_clue
SELECT id,
       create_date_time,
       update_date_time,
       deleted,
       customer_id,
       customer_relationship_id,
       session_id,
       sid,
       status,
       `user`,
       create_time,
       platform,
       s_name,
       seo_source,
       seo_keywords,
       ip,
       referrer,
       from_url,
       landing_page_url,
       url_title,
       to_peer,
       manual_time,
       begin_time,
       reply_msg_count,
       total_msg_count,
       msg_count,
       comment,
       finish_reason,
       finish_user,
       end_time,
       platform_description,
       browser_name,
       os_info,
       area,
       country,
       province,
       city,
       creator,
       name,
       idcard,
       phone,
       itcast_school_id,
       itcast_school,
       itcast_subject_id,
       itcast_subject,
       wechat,
       qq,
       email,
       gender,
       level,
       origin_type,
       information_way,
       working_years,
       technical_directions,
       customer_state,
       valid,
       anticipat_signup_date,
       clue_state,
       if(clue_state='VALID_NEW_CLUES',1,0) AS clue_state_stat,
       scrm_department_id,
       superior_url,
       superior_source,
       landing_url,
       landing_source,
       info_url,
       info_source,
       origin_channel,
       course_id,
       course_name,
       zhuge_session_id,
       is_repeat,
       tenant,
       activity_id,
       activity_name,
       follow_type,
       shunt_mode_id,
       shunt_employee_group_id
FROM edu_ods.customer_clue;

--todo 插入数据到dim_employee  员工信息维度表
INSERT INTO edu_dwd.dim_employee
SELECT id,
       email,
       real_name,
       phone,
       department_id,
       department_name,
       remote_login,
       job_number,
       cross_school,
       last_login_date,
       creator,
       create_date_time,
       update_date_time,
       deleted,
       scrm_department_id,
       leave_office,
       leave_office_time,
       reinstated_time,
       superior_leaders_id,
       tdepart_id,
       tenant,
       ems_user_name
FROM edu_ods.employee;

--todo 插入数据到dim_itcast_school  校区维度表
INSERT INTO edu_dwd.dim_itcast_school
SELECT id, create_date_time, update_date_time, deleted, name, code, tenant
FROM edu_ods.itcast_school;

--todo 插入数据到dim_itcast_subject学科维度表
INSERT INTO edu_dwd.dim_itcast_subject
SELECT id, create_date_time, update_date_time, deleted, name, code, tenant
FROM edu_ods.itcast_subject;

--todo 插入数据到dim_scrm_department员工部门维度表
INSERT INTO edu_dwd.dim_scrm_department
SELECT id,
       name,
       parent_id,
       create_date_time,
       update_date_time,
       deleted,
       id_path,
       tdepart_code,
       creator,
       depart_level,
       depart_sign,
       depart_line,
       depart_sort,
       disable_flag,
       tenant
FROM edu_ods.scrm_department;



