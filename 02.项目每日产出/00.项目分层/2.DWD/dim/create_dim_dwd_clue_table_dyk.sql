--todo dim_custmoer  客户静态信息维度表
CREATE TABLE IF NOT EXISTS dim_customer (
  id INT COMMENT '自增ID',
  customer_relationship_id INT COMMENT '当前意向id',
  create_date_time TIMESTAMP COMMENT '创建时间',
  update_date_time TIMESTAMP COMMENT '最后更新时间',
  deleted BOOLEAN COMMENT '是否被删除（禁用），默认为false',
  name STRING COMMENT '姓名',
  idcard STRING COMMENT '身份证号',
  birth_year INT COMMENT '出生年份',
  gender STRING COMMENT '性别，默认为"MAN"',
  phone STRING COMMENT '手机号',
  wechat STRING COMMENT '微信',
  qq STRING COMMENT 'qq号',
  email STRING COMMENT '邮箱',
  area STRING COMMENT '所在区域',
  leave_school_date DATE COMMENT '离校时间',
  graduation_date DATE COMMENT '毕业时间',
  bxg_student_id STRING COMMENT '博学谷学员ID，可能未关联到，不存在',
  creator INT COMMENT '创建人ID',
  origin_type STRING COMMENT '数据来源',
  origin_channel STRING COMMENT '来源渠道',
  tenant INT COMMENT '租户ID，默认为0',
  md_id INT COMMENT '中台id，默认为0'
)
COMMENT '客户静态信息维度表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');


--todo dim_customer_clue客户线索维度表
CREATE TABLE IF NOT EXISTS dim_customer_clue (
  id INT COMMENT '自增ID，需在数据导入时自动生成',
  create_date_time TIMESTAMP COMMENT '创建时间',
  update_date_time TIMESTAMP COMMENT '最后更新时间，需在数据更新时自行维护',
  deleted BOOLEAN COMMENT '是否被删除（禁用），默认为false',
  customer_id INT COMMENT '客户id',
  customer_relationship_id INT COMMENT '客户关系id',
  session_id STRING COMMENT '七陌会话id',
  sid STRING COMMENT '访客id',
  status STRING COMMENT '状态（undeal待领取 deal 已领取 finish 已关闭 changePeer 已流转）',
  `user` STRING COMMENT '所属坐席',
  create_time TIMESTAMP COMMENT '七陌创建时间',
  platform STRING COMMENT '平台来源 （pc-网站咨询|wap-wap咨询|sdk-app咨询|weixin-微信咨询）',
  s_name STRING COMMENT '用户名称',
  seo_source STRING COMMENT '搜索来源',
  seo_keywords STRING COMMENT '关键字',
  ip STRING COMMENT 'IP地址',
  referrer STRING COMMENT '上级来源页面',
  from_url STRING COMMENT '会话来源页面',
  landing_page_url STRING COMMENT '访客着陆页面',
  url_title STRING COMMENT '咨询页面title',
  to_peer STRING COMMENT '所属技能组',
  manual_time TIMESTAMP COMMENT '人工开始时间',
  begin_time TIMESTAMP COMMENT '坐席领取时间',
  reply_msg_count INT COMMENT '客服回复消息数',
  total_msg_count INT COMMENT '消息总数',
  msg_count INT COMMENT '客户发送消息数',
  comment STRING COMMENT '备注',
  finish_reason STRING COMMENT '结束类型',
  finish_user STRING COMMENT '结束坐席',
  end_time TIMESTAMP COMMENT '会话结束时间',
  platform_description STRING COMMENT '客户平台信息',
  browser_name STRING COMMENT '浏览器名称',
  os_info STRING COMMENT '系统名称',
  area STRING COMMENT '区域',
  country STRING COMMENT '所在国家',
  province STRING COMMENT '省',
  city STRING COMMENT '城市',
  creator INT COMMENT '创建人',
  name STRING COMMENT '客户姓名',
  idcard STRING COMMENT '身份证号',
  phone STRING COMMENT '手机号',
  itcast_school_id INT COMMENT '校区Id',
  itcast_school STRING COMMENT '校区',
  itcast_subject_id INT COMMENT '学科Id',
  itcast_subject STRING COMMENT '学科',
  wechat STRING COMMENT '微信',
  qq STRING COMMENT 'qq号',
  email STRING COMMENT '邮箱',
  gender STRING COMMENT '性别，默认为"MAN"',
  level STRING COMMENT '客户级别',
  origin_type STRING COMMENT '数据来源渠道',
  information_way STRING COMMENT '资讯方式',
  working_years DATE COMMENT '开始工作时间',
  technical_directions STRING COMMENT '技术方向',
  customer_state STRING COMMENT '当前客户状态',
  valid BOOLEAN COMMENT '该线索是否是网资有效线索，默认为false',
  anticipat_signup_date DATE COMMENT '预计报名时间',
  clue_state STRING COMMENT '线索状态，默认为"NOT_SUBMIT"',
-- 下面的需要用clue_state转换
clue_state_stat int COMMENT '新老用户，0代表老用户，1代表用户，VALID_NEW_CLUES为新用户，其他都定为老用户',
  --
  scrm_department_id INT COMMENT 'SCRM内部部门id',
  superior_url STRING COMMENT '逐个获取上级页面URL',
  superior_source STRING COMMENT '逐个获取上级页面URL标题',
  landing_url STRING COMMENT '逐个获取着陆页面URL',
  landing_source STRING COMMENT '逐个获取着陆页面URL来源',
  info_url STRING COMMENT '逐个获取留咨页URL',
  info_source STRING COMMENT '逐个获取留咨页URL标题',
  origin_channel STRING COMMENT '投放渠道',
  course_id INT COMMENT '课程id',
  course_name STRING COMMENT '课程名称',
  zhuge_session_id STRING COMMENT '诸葛会话id',
  is_repeat INT COMMENT '是否重复线索(手机号维度) 0:正常 1：重复，默认为0',
  tenant INT COMMENT '租户id，默认为0',
  activity_id STRING COMMENT '活动id',
  activity_name STRING COMMENT '活动名称',
  follow_type INT COMMENT '分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取，默认为0',
  shunt_mode_id INT COMMENT '匹配到的技能组id',
  shunt_employee_group_id INT COMMENT '所属分流员工组'
)
COMMENT '客户线索维度表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');


--todo dim_employee  员工信息维度表

CREATE TABLE IF NOT EXISTS dim_employee (
  id INT COMMENT '自增主键ID，需外部生成',
  email STRING COMMENT '公司邮箱，OA登录账号',
  real_name STRING COMMENT '员工的真实姓名',
  phone STRING COMMENT '手机号',
  department_id STRING COMMENT 'OA中的部门编号，有负值',
  department_name STRING COMMENT 'OA中的部门名',
  remote_login BOOLEAN COMMENT '员工是否可以远程登录',
  job_number STRING COMMENT '员工工号',
  cross_school BOOLEAN COMMENT '是否有跨校区权限',
  last_login_date TIMESTAMP COMMENT '最后登录日期',
  creator INT COMMENT '创建人',
  create_date_time TIMESTAMP COMMENT '创建时间',
  update_date_time TIMESTAMP COMMENT '最后更新时间，Hive中不支持ON UPDATE CURRENT_TIMESTAMP',
  deleted BOOLEAN COMMENT '是否被删除（禁用），默认为false',
  scrm_department_id INT COMMENT 'SCRM内部部门id',
  leave_office BOOLEAN COMMENT '离职状态',
  leave_office_time TIMESTAMP COMMENT '离职时间',
  reinstated_time TIMESTAMP COMMENT '复职时间',
  superior_leaders_id INT COMMENT '上级领导ID',
  tdepart_id INT COMMENT '直属部门',
  tenant INT COMMENT '租户，默认为0',
  ems_user_name STRING COMMENT 'EMS用户名'
)
COMMENT '员工信息维度表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');


--todo dim_itcast_school  校区维度表
CREATE TABLE IF NOT EXISTS dim_itcast_school (
  id INT COMMENT '自增主键ID，需外部生成',
  create_date_time TIMESTAMP COMMENT '创建时间',
  update_date_time TIMESTAMP COMMENT '最后更新时间，Hive中不支持ON UPDATE CURRENT_TIMESTAMP',
  deleted BOOLEAN COMMENT '是否被删除（禁用），默认为false',
  name STRING COMMENT '校区名称',
  code STRING COMMENT '校区代码',
  tenant INT COMMENT '租户，默认为0'
)
COMMENT '校区维度表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--todo dim_itcast_subject学科维度表
CREATE TABLE IF NOT EXISTS dim_itcast_subject (
  id INT COMMENT '自增主键ID，需外部生成',
  create_date_time TIMESTAMP COMMENT '创建时间',
  update_date_time TIMESTAMP COMMENT '最后更新时间，Hive中不支持ON UPDATE CURRENT_TIMESTAMP 功能需外部处理',
  deleted BOOLEAN COMMENT '是否被删除（禁用），默认为false',
  name STRING COMMENT '学科名称',
  code STRING COMMENT '学科代码，允许为空',
  tenant INT COMMENT '租户ID，默认为0'
)
COMMENT '学科维度表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--todo dim_scrm_department员工部门维度表
CREATE TABLE IF NOT EXISTS dim_scrm_department (
  id INT COMMENT '部门id',
  name STRING COMMENT '部门名称',
  parent_id INT COMMENT '父部门id',
  create_date_time TIMESTAMP COMMENT '创建时间，默认为当前时间戳',
  update_date_time TIMESTAMP COMMENT '更新时间，默认为当前时间戳并在每次更新时刷新',
  deleted BOOLEAN COMMENT '删除标志，默认为false',
  id_path STRING COMMENT '编码全路径',
  tdepart_code INT COMMENT '直属部门编码',
  creator STRING COMMENT '创建者',
  depart_level INT COMMENT '部门层级',
  depart_sign INT COMMENT '部门标志，暂时默认为1',
  depart_line INT COMMENT '业务线，存储业务线编码',
  depart_sort INT COMMENT '排序字段',
  disable_flag INT COMMENT '禁用标志',
  tenant INT COMMENT '租户id，默认为0'
)
COMMENT '员工部门维度表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');