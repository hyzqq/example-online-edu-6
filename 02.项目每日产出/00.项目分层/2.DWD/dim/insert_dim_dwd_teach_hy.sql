--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;

--班级作息表
insert overwrite table edu_dwd.dim_tbh_class_time_table partition (export_date)
select *
from edu_ods.tbh_class_time_table;

--班级排课
insert overwrite table edu_dwd.dim_course_table_upload_detail partition (export_date)
select *
from edu_ods.course_table_upload_detail;
-- 在读学员信息
insert overwrite table edu_dwd.dim_class_studying_student_count partition (export_date)
select*
from edu_ods.class_studying_student_count;