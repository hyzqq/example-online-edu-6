insert overwrite table edu_dwd.dwd_dim_customer
select *
from edu_ods.ods_customer
where deleted=0 ;--数据是否被禁用，为1被禁用，为0没有被禁用

insert overwrite table edu_dwd.dwd_dim_customer_appeal
select *
from edu_ods.ods_customer_appeal
where deleted=0 ;--数据是否被禁用，为1被禁用，为0没有被禁用


insert overwrite table edu_dwd.dwd_dim_employee
select *
from edu_ods.ods_employee
where  deleted=0; --数据是否被禁用，为1被禁用，为0没有被禁用


insert overwrite table edu_dwd.dwd_dim_itcast_clazz
select *
from edu_ods.ods_itcast_clazz
where  deleted=0; --数据是否被禁用，为1被禁用，为0没有被禁用


insert overwrite table edu_dwd.dwd_dim_itcast_school
select *
from edu_ods.ods_itcast_school
where  deleted=0; --数据是否被禁用，为1被禁用，为0没有被禁用

insert overwrite table edu_dwd.dwd_dim_itcast_subject
select *
from edu_ods.ods_itcast_subject
where  deleted=0; --数据是否被禁用，为1被禁用，为0没有被禁用

insert overwrite table edu_dwd.dwd_dim_scrm_department
select *
from edu_ods.ods_scrm_department
where  deleted=0; --数据是否被禁用，为1被禁用，为0没有被禁用