--维度表

CREATE TABLE edu_dwd.dim_class_studying_student_count
(
    id                     INT ,
    school_id              INT COMMENT '校区id',
    subject_id             INT   COMMENT '学科id',
    class_id               INT   COMMENT '班级id',
    studying_student_count INT   COMMENT '在读班级人数',
    studying_date         timestamp COMMENT '在读日期',
    end_date string
)comment '在读学员人数信息表'
partitioned by(dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');


CREATE TABLE edu_dwd.dim_course_table_upload_detail
(
    id                  INT  COMMENT 'id',
    base_id             INT          COMMENT '课程主表id',
    class_id            INT           COMMENT '班级id',
    class_date          DATE           COMMENT '上课日期',
    content             string COMMENT '课程内容',
    teacher_id          INT           COMMENT '老师id',
    teacher_name      string  COMMENT '老师名字',
    job_number         string    COMMENT '工号',
    classroom_id        INT         COMMENT '教室id',
    classroom_name      string    COMMENT '教室名称',
    is_outline          INT        COMMENT '是否大纲 0 否 1 是',
    class_mode          INT       COMMENT '上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播',
    is_stage_exam       INT        COMMENT '是否阶段考试（0：否 1：是）',
    is_pay              INT       COMMENT '代课费（0：无 1：有）',
    tutor_teacher_id    INT    COMMENT '晚自习辅导老师id',
    tutor_teacher_name  string COMMENT '辅导老师姓名',
    tutor_job_number    string COMMENT '晚自习辅导老师工号',
    is_subsidy          INT   COMMENT '晚自习补贴（0：无 1：有）',
    answer_teacher_id   INT        COMMENT '答疑老师id',
    answer_teacher_name string COMMENT '答疑老师姓名',
    answer_job_number string comment '答疑老师工号',
    remark            string COMMENT '备注',
    create_time         timestamp    COMMENT '创建时间',
    end_date string
)comment '班级排课信息表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

CREATE TABLE edu_dwd.dim_tbh_class_time_table
(
    id                    INT  COMMENT '主键id',
    class_id              INT        COMMENT '班级id',
    morning_template_id   INT        COMMENT '上午出勤模板id',
    morning_begin_time  string        COMMENT '上午开始时间',
    morning_end_time      string        COMMENT '上午结束时间',
    afternoon_template_id  INT   COMMENT '下午出勤模板id',
    afternoon_begin_time string     COMMENT '下午开始时间',
    afternoon_end_time    string        COMMENT '下午结束时间',
    evening_template_id   INT      COMMENT '晚上出勤模板id',
    evening_begin_time   string      COMMENT '晚上开始时间',
    evening_end_time     string   COMMENT '晚上结束时间',
    use_begin_date      date       COMMENT '使用开始日期',
    use_end_date         timestamp      COMMENT '使用结束日期',
    create_time          timestamp     COMMENT '创建时间',
    create_person         INT         COMMENT '创建人',
    remark               string  COMMENT '备注',
    end_date string
)comment '班级作息时间表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');
