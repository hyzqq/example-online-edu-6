--客户静态信息表
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_customer (
  `id` INT COMMENT '学员id',
  `customer_relationship_id` INT COMMENT '当前意向id',
  `create_date_time` TIMESTAMP COMMENT '创建时间',
  `update_date_time` TIMESTAMP COMMENT '最后更新时间',
  `deleted` int COMMENT '是否被删除（禁用）',
  `name` STRING COMMENT '姓名',
  `idcard` STRING COMMENT '身份证号',
  `birth_year` INT COMMENT '出生年份',
  `gender` STRING COMMENT '性别',
  `phone` STRING COMMENT '手机号',
  `wechat` STRING COMMENT '微信',
  `qq` STRING COMMENT 'qq号',
  `email` STRING COMMENT '邮箱',
  `area` STRING COMMENT '所在区域',
  `leave_school_date` DATE COMMENT '离校时间',
  `graduation_date` DATE COMMENT '毕业时间',
  `bxg_student_id` STRING COMMENT '博学谷学员ID',
  `creator` INT COMMENT '创建人ID',
  `origin_type` STRING COMMENT '数据来源',
  `origin_channel` STRING COMMENT '来源渠道',
  `tenant` INT COMMENT '租户',
  `md_id` INT COMMENT '中台id'
)
COMMENT '客户静态信息表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--线索申诉信息表
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_customer_appeal (
  `id` INT COMMENT '主键',
  `customer_relationship_first_id` INT COMMENT '第一条客户关系id',
  `employee_id` INT COMMENT '申诉人',
  `employee_name` STRING COMMENT '申诉人姓名',
  `employee_department_id` INT COMMENT '申诉人部门',
  `employee_tdepart_id` INT COMMENT '申诉人所属部门',
  `appeal_status` INT COMMENT '申诉状态，0:待稽核 1:无效 2：有效',
  `audit_id` INT COMMENT '稽核人id',
  `audit_name` STRING COMMENT '稽核人姓名',
  `audit_department_id` INT COMMENT '稽核人所在部门',
  `audit_department_name` STRING COMMENT '稽核人部门名称',
  `audit_date_time` TIMESTAMP COMMENT '稽核时间',
  `create_date_time` TIMESTAMP COMMENT '创建时间（申诉时间）',
  `update_date_time` TIMESTAMP COMMENT '更新时间',
  `deleted` int COMMENT '删除标志位',
  `tenant` INT COMMENT '租户'
)
COMMENT '线索申诉信息表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');


--员工信息表
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_employee (
  `id` INT COMMENT '员工id',
  `email` STRING COMMENT '公司邮箱，OA登录账号',
  `real_name` STRING COMMENT '员工的真实姓名',
  `phone` STRING COMMENT '手机号，目前还没有使用；隐私问题OA接口没有提供这个属性',
  `department_id` STRING COMMENT 'OA中的部门编号，有负值',
  `department_name` STRING COMMENT 'OA中的部门名',
  `remote_login` int COMMENT '员工是否可以远程登录',
  `job_number` STRING COMMENT '员工工号',
  `cross_school` int COMMENT '是否有跨校区权限',
  `last_login_date` TIMESTAMP COMMENT '最后登录日期',
  `creator` INT COMMENT '创建人',
  `create_date_time` TIMESTAMP COMMENT '创建时间',
  `update_date_time` TIMESTAMP COMMENT '最后更新时间',
  `deleted` int COMMENT '是否被删除（禁用）',
  `scrm_department_id` INT COMMENT 'SCRM内部部门id',
  `leave_office` int COMMENT '离职状态',
  `leave_office_time` TIMESTAMP COMMENT '离职时间',
  `reinstated_time` TIMESTAMP COMMENT '复职时间',
  `superior_leaders_id` INT COMMENT '上级领导ID',
  `tdepart_id` INT COMMENT '直属部门',
  `tenant` INT COMMENT '租户',
  `ems_user_name` STRING COMMENT '用户名'
)
COMMENT '员工信息表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--班级信息表
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_itcast_clazz (
  `id` INT COMMENT '课程id(非自增)',
  `create_date_time` TIMESTAMP COMMENT '创建时间',
  `update_date_time` TIMESTAMP COMMENT '最后更新时间',
  `deleted` int COMMENT '是否被删除（禁用）',
  `itcast_school_id` STRING COMMENT '校区ID',
  `itcast_school_name` STRING COMMENT '校区名称',
  `itcast_subject_id` STRING COMMENT '学科ID',
  `itcast_subject_name` STRING COMMENT '学科名称',
  `itcast_brand` STRING COMMENT '品牌',
  `clazz_type_state` STRING COMMENT '班级类型状态',
  `clazz_type_name` STRING COMMENT '班级类型名称',
  `teaching_mode` STRING COMMENT '授课模式',
  `start_time` TIMESTAMP COMMENT '开班时间',
  `end_time` TIMESTAMP COMMENT '毕业时间',
  `comment` STRING COMMENT '备注',
  `detail` STRING COMMENT '详情(比如：27期)',
  `uncertain` int COMMENT '待定班(0:否,1:是)',
  `tenant` INT COMMENT '租户'
)
COMMENT '班级信息表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--校区信息表
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_itcast_school (
  `id` INT COMMENT '校区id',
  `create_date_time` TIMESTAMP COMMENT '创建时间',
  `update_date_time` TIMESTAMP COMMENT '最后更新时间',
  `deleted` int COMMENT '是否被删除（禁用）',
  `name` STRING COMMENT '校区名称',
  `code` STRING comment '校区代号',
  `tenant` INT COMMENT '租户'
)
COMMENT '校区信息表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--学科信息
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_itcast_subject (
  `id` INT COMMENT '学科id',
  `create_date_time` TIMESTAMP COMMENT '创建时间',
  `update_date_time` TIMESTAMP COMMENT '最后更新时间',
  `deleted` int COMMENT '是否被删除（禁用）',
  `name` STRING COMMENT '学科名称',
  `code` STRING COMMENT '学科代号',
  `tenant` INT COMMENT '租户'
)
COMMENT '学科信息'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');

--员工部门表
CREATE TABLE IF NOT EXISTS edu_dwd.dwd_dim_scrm_department (
  `id` INT COMMENT '部门id',
  `name` STRING COMMENT '部门名称',
  `parent_id` INT COMMENT '父部门id',
  `create_date_time` TIMESTAMP COMMENT '创建时间',
  `update_date_time` TIMESTAMP COMMENT '更新时间',
  `deleted` int COMMENT '删除标志',
  `id_path` STRING COMMENT '编码全路径',
  `tdepart_code` INT COMMENT '直属部门',
  `creator` STRING COMMENT '创建者',
  `depart_level` INT COMMENT '部门层级',
  `depart_sign` INT COMMENT '部门标志，暂时默认1',
  `depart_line` INT COMMENT '业务线，存储业务线编码',
  `depart_sort` INT COMMENT '排序字段',
  `disable_flag` INT COMMENT '禁用标志',
  `tenant` INT COMMENT '租户'
)
COMMENT '员工部门表'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS ORC
TBLPROPERTIES ('orc.compress'='SNAPPY');