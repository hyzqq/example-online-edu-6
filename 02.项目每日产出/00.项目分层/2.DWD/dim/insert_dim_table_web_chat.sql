insert overwrite table edu_dwd.dwd_dim_area
select
    id,
    area,
    country,
    province,
    city
from edu_ods.ods_web_chat_ems_2019_07;

insert overwrite table edu_dwd.dwd_dim_date
select
    id,
    substring(create_date_time,1,19) as create_date_time,
    substring(create_time,1,19) as create_time,
    substring(create_time,1,13) as date_hour
from edu_ods.ods_web_chat_ems_2019_07;
