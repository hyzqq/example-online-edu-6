--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;

--班级作息表
insert overwrite table edu_dwd.dim_tbh_class_time_table
select
id,
class_id,
morning_template_id,
morning_begin_time,
morning_end_time,
afternoon_template_id,
afternoon_begin_time,
afternoon_end_time,
evening_template_id,
evening_begin_time,
evening_end_time,
use_begin_date,
use_end_date,
create_time
from edu_ods.tbh_class_time_table;

--班级排课
insert overwrite table edu_dwd.dim_course_table_upload_detail
select id,
       base_id,
       class_id,
       class_date,
       content,
       create_time
from edu_ods.course_table_upload_detail;
-- 在读学员信息
insert overwrite table edu_dwd.dim_class_studying_student_count
select
       id,
       school_id,
       subject_id,
       class_id,
       studying_student_count,
       studying_date
from edu_ods.class_studying_student_count;

