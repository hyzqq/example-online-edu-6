insert into hive.edu_dws.dws_web_chat_ems_daycount
with temp as (select
-- 维度
    dt,
    date_hour,
    country,
    province,
    city,
    origin_channel,
    seo_source,
    from_url,
-- 指标
    sid,
    session_id,
    ip,
-- 判断条件
    msg_count,-- 大于等于一即说明有咨询
-- 去重标记
    row_number() over(partition by sid) as sid_rn,
    row_number() over(partition by sid,ip) as ip_rn,
    row_number() over(partition by sid,ip,session_id) as session_rn
from hive.edu_dwb.dwb_web_chat_ems_detail)
select
    date_hour,
    case when grouping(country) = 0
            then country
            else null end as country,
    case when grouping(province) = 0
            then province
            else null end as province,
    case when grouping(city) = 0
            then city
            else null end as city,
    case when grouping(origin_channel) = 0
            then origin_channel
            else null end as origin_channel,
    case when grouping(seo_source) = 0
            then seo_source
            else null end as seo_source,
    case when grouping(from_url) = 0
            then from_url
            else null end as from_url,
-- 分组标记
    case when grouping(dt,date_hour,country,province,city) = 0
            then 'city'
         when grouping(dt,date_hour,country,province) = 0
            then 'province'
         when grouping(dt,date_hour,country) = 0
            then 'country'
         when grouping(dt,date_hour,origin_channel) = 0
            then 'origin_channel'
         when grouping(dt,date_hour,seo_source) = 0
            then 'seo_source'
         when grouping(dt,date_hour,from_url) = 0
            then 'from_url'
         when grouping(dt,date_hour) = 0
            then 'all'
         else 'other' end as group_type,
-- 访问用户量 user_cnt
    case when grouping(dt,date_hour,country,province,city) = 0
            then count(if(city is not null and sid_rn = 1,sid,null))
         when grouping(dt,date_hour,country,province) = 0
            then count(if(province is not null and sid_rn = 1,sid,null))
         when grouping(dt,date_hour,country) = 0
            then count(if(country is not null and sid_rn = 1,sid,null))
         when grouping(dt,date_hour,origin_channel) = 0
            then count(if(origin_channel is not null and sid_rn = 1,sid,null))
         when grouping(dt,date_hour,seo_source) = 0
            then count(if(seo_source is not null and sid_rn = 1,sid,null))
         when grouping(dt,date_hour,from_url) = 0
            then count(if(from_url is not null and sid_rn = 1,sid,null))
         when grouping(dt,date_hour) = 0
            then count(if(sid is not null and sid_rn = 1,sid,null))
         else null end as user_cnt,
-- ip个数
    case when grouping(dt,date_hour,country,province,city) = 0
            then count(if(city is not null and ip_rn = 1,ip,null))
         when grouping(dt,date_hour,country,province) = 0
            then count(if(province is not null and ip_rn = 1,ip,null))
         when grouping(dt,date_hour,country) = 0
            then count(if(country is not null and ip_rn = 1,ip,null))
         when grouping(dt,date_hour,origin_channel) = 0
            then count(if(origin_channel is not null and ip_rn = 1,ip,null))
         when grouping(dt,date_hour,seo_source) = 0
            then count(if(seo_source is not null and ip_rn = 1,ip,null))
         when grouping(dt,date_hour,from_url) = 0
            then count(if(from_url is not null and ip_rn = 1,ip,null))
         when grouping(dt,date_hour) = 0
            then count(if(sid is not null and ip_rn = 1,ip,null))
         else null end as ip_cnt,
-- 访问会话个数
    case when grouping(dt,date_hour,country,province,city) = 0
            then count(if(city is not null and session_rn = 1,session_id,null))
         when grouping(dt,date_hour,country,province) = 0
            then count(if(province is not null and session_rn = 1,session_id,null))
         when grouping(dt,date_hour,country) = 0
            then count(if(country is not null and session_rn = 1,session_id,null))
         when grouping(dt,date_hour,origin_channel) = 0
            then count(if(origin_channel is not null and session_rn = 1,session_id,null))
         when grouping(dt,date_hour,seo_source) = 0
            then count(if(seo_source is not null and session_rn = 1,session_id,null))
         when grouping(dt,date_hour,from_url) = 0
            then count(if(from_url is not null and session_rn = 1,session_id,null))
         when grouping(dt,date_hour) = 0
            then count(if(sid is not null and session_rn = 1,session_id,null))
         else null end as session_cnt,
-- 咨询总人数
    case when grouping(dt,date_hour,country,province,city) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and city is not null,sid,null))
         when grouping(dt,date_hour,country,province) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and province is not null,sid,null))
         when grouping(dt,date_hour,country) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and country is not null,sid,null))
         when grouping(dt,date_hour,origin_channel) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and origin_channel is not null,sid,null))
         when grouping(dt,date_hour,seo_source) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and seo_source is not null,sid,null))
         when grouping(dt,date_hour,from_url) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and from_url is not null,sid,null))
         when grouping(dt,date_hour) = 0
            then count(if(msg_count > 0 and sid_rn = 1 and sid is not null and date_hour is not null,sid,null))
         else null end as consul_cnt,
    dt
from temp
group by
    grouping sets(
    (dt,date_hour),
    (dt,date_hour,country),
    (dt,date_hour,country,province),
    (dt,date_hour,country,province,city),
    (dt,date_hour,origin_channel),
    (dt,date_hour,seo_source),
    (dt,date_hour,from_url)
    );


