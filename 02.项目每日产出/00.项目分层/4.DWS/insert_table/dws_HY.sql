WITH
t1 AS (
    -- 预处理数据：选择必要的字段并进行类型转换，确保数据符合后续处理的格式要求。
    SELECT
        record_id,
        time_table_id,
        record_class_id,
        student_id,
        -- 将签到时间从字符串转换为TIME类型，以便进行时间比较。
        CAST(SUBSTRING(cast(signin_time AS VARCHAR), 12, 8) AS TIME) AS signin_time,
        signin_date,
        share_state,
        end_date,
        count_id,
        count_class_id,
        studying_student_count,
        studying_date,
        course_id,
        course_class_id,
        class_date,
        content,
        time_id,
        time_class_id,
        -- 同样将其他时间字段转换为TIME类型。
        CAST(morning_begin_time AS TIME) AS morning_begin_time,
        CAST(morning_end_time AS TIME) AS morning_end_time,
        CAST(afternoon_begin_time AS TIME) AS afternoon_begin_time,
        CAST(afternoon_end_time AS TIME) AS afternoon_end_time,
        CAST(evening_begin_time AS TIME) AS evening_begin_time,
        CAST(evening_end_time AS TIME) AS evening_end_time,
        use_begin_date,
        use_end_date,
        start_date
    FROM hive.edu_dwb.tbh_student_signin_record_detail
    WHERE class_date IS NOT NULL AND content IS NOT NULL AND content != '开学典礼'
    -- 过滤掉无效或特定事件的数据，确保数据质量。
),
t2 AS (
    -- 计算每个学生在各个时间段的签到状态，分为按时、迟到等。
    SELECT
        record_id,
        time_table_id,
        record_class_id,
        student_id,
        signin_time,
        -- 早晨按时签到：在规定时间范围内签到视为按时。
        CASE
            WHEN signin_time BETWEEN (morning_begin_time - interval '40' minute)
                        AND (morning_begin_time + interval '10' minute) THEN '1'
        END AS morning_yes,
        -- 早晨迟到：在规定开始时间后签到，但在结束时间前视为迟到。
        CASE
            WHEN signin_time > (morning_begin_time + interval '10' MINUTE)
                        AND signin_time <= morning_end_time THEN '1'
        END AS morning_miss,
        -- 下午按时签到：类似早晨的判断逻辑。
        CASE
            WHEN signin_time BETWEEN (afternoon_begin_time - interval '40' minute)
                        AND (afternoon_begin_time + interval '10' minute) THEN '1'
        END AS afternoon_yes,
        -- 下午迟到
        CASE
            WHEN signin_time > (afternoon_begin_time + interval '10' minute)
                        AND signin_time <= afternoon_end_time THEN '1'
        END AS afternoon_miss,
        -- 晚上按时签到
        CASE
            WHEN signin_time BETWEEN (evening_begin_time - interval '40' minute)
                        AND (evening_begin_time + interval '10' minute) THEN '1'
        END AS evening_yes,
        -- 晚上迟到
        CASE
            WHEN signin_time > (evening_begin_time + interval '10' MINUTE)
                        AND signin_time <= evening_end_time THEN '1'
        END AS evening_miss,
        signin_date,
        share_state,
        count_class_id,
        studying_student_count,
        studying_date,
        course_id,
        course_class_id,
        class_date,
        content,
        time_id,
        time_class_id,
        t1.morning_begin_time,
        t1.morning_end_time,
        t1.afternoon_begin_time,
        t1.afternoon_end_time,
        t1.evening_begin_time,
        t1.evening_end_time,
        use_begin_date,
        use_end_date,
        start_date
    FROM t1
    -- 从预处理的CTE t1 中选择数据进行进一步的状态标记。
),
t3 AS (
    -- 对每个学生的签到情况进行聚合计算，计算每个时间段的总签到情况。
    SELECT
        record_class_id,
        class_date,
        student_id,
        SUM(CAST(morning_yes AS INT)) AS morning_yes, -- 将字符转为整数进行求和。
        SUM(CAST(morning_miss AS INT)) AS morning_miss,
        SUM(CAST(afternoon_yes AS INT)) AS afternoon_yes,
        SUM(CAST(afternoon_miss AS INT)) AS afternoon_miss,
        SUM(CAST(evening_yes AS INT)) AS evening_yes,
        SUM(CAST(evening_miss AS INT)) AS evening_miss
    FROM t2
    GROUP BY record_class_id, class_date, student_id
    -- 按班级、日期和学生ID进行分组，以便计算每个学生每天的签到状态。
),
t4 AS (
    -- 最后的处理步骤，转换聚合的结果为0或1，代表最终的出勤状态。
    SELECT
        record_class_id,
        class_date,
        student_id,
        IF(morning_yes IS NOT NULL, 1, 0) AS morning_yes, -- 如果有签到记录则标记为1，否则为0。
        IF(morning_yes IS NULL AND morning_miss IS NOT NULL, 1, 0) AS morning_miss,-- 如果签到记录有正常打卡的，就不计算为迟到
        IF(afternoon_yes IS NOT NULL, 1, 0) AS afternoon_yes,
        IF(afternoon_yes IS NULL AND afternoon_miss IS NOT NULL, 1, 0) AS afternoon_miss,
        IF(evening_yes IS NOT NULL, 1, 0) AS evening_yes,
        IF(evening_yes IS NULL AND evening_miss IS NOT NULL, 1, 0) AS evening_miss
    FROM t3
)
-- 最终选择查询结果，包括班级ID、日期和每个时间段的最终出勤状态。
SELECT
    record_class_id,
    class_date,
    SUM(morning_yes) AS morning_yes,
    SUM(morning_miss) AS morning_miss,
    SUM(afternoon_yes) AS afternoon_yes,
    SUM(afternoon_miss) AS afternoon_miss,
    SUM(evening_yes) AS evening_yes,
    SUM(evening_miss) AS evening_miss
FROM t4
WHERE class_date IS NOT NULL
GROUP BY record_class_id, class_date
-- 对最终的数据进行汇总，计算每个班级每天的总出勤情况。
