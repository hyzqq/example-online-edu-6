-- todo 每天每年每月线上线下各个校区各个学科的报名人数、意向人数、有效线索人数
insert into  hive.edu_dws.dws_customer_detail_cnt
with a as (
select
--维度
a."day",--天，ID
a.itcast_school_id,a.sc_name,--学校
a.itcast_subject_id,a.name,--学科
if (a.origin_type_state<>-1,a.origin_type_state,null) as origin_type_state,--线上线下,1线上，0为线下
a.origin_channel,--来源渠道
a.tdepart_id,a.depart_name,--咨询中心
--指标
a.id,--意向ID
--报名判断
if(a.payment_state='PAID',1,null) as payment_state,--支付状态,1为支付
--有效线索判断
b.appeal_status,
--分组去重
row_number() over(partition by a.id) as id_rn,
row_number() over(partition by a."day") as id_rn0,
row_number() over(partition by a.id,a.itcast_school_id,a.sc_name) as id_rn1,
row_number() over(partition by a.id,a.origin_type_state,a.itcast_school_id,a.sc_name) as id_rn2,
row_number() over(partition by a.id,a.origin_type_state,a.itcast_subject_id,a.name) as id_rn3,
row_number() over(partition by a.id,a.origin_type_state,a.itcast_school_id,a.sc_name,a.itcast_subject_id,a.name)  as id_rn4,
row_number() over(partition by a.id,a.origin_type_state,a.origin_channel) as id_rn5,
row_number() over(partition by a.id,a.origin_type_state,a.tdepart_id,a.depart_name) as id_rn6,
row_number() over(partition by a.id,a.origin_type_state) as id_rn7
from hive.edu_dwb.dwb_customer_information_detail a left join hive.edu_dwb.dwb_customer_clue_detail b
on a.id = b.cl_customer_relationship_id
)
select
--维度查询返回
case when grouping (appeal_status) =0
then appeal_status
else null end as appeal_status,
case when grouping (payment_state)=0
then payment_state
else null end as payment_state ,
case when grouping (itcast_school_id) =0
then itcast_school_id
else null end as itcast_school_id,
case when grouping (itcast_school_id) =0
then sc_name
else null end as sc_name,
case when grouping (itcast_subject_id) =0
then itcast_subject_id
else null end as itcast_subject_id,
case when grouping (itcast_subject_id) =0
then name
else null end as name,
case when grouping (origin_type_state) =0
then origin_type_state
else null end as origin_type_state,
case when grouping (origin_channel) =0
then origin_channel
else null end as origin_channel,
case when grouping (tdepart_id) =0
then tdepart_id
else null end as tdepart_id,
case when grouping (tdepart_id) =0
then depart_name
else null end as depart_name,
case when grouping (a."day")=0
then a."day"
else null end as "day",
--分组类型
case when grouping("day",itcast_school_id,sc_name,payment_state)=0
then 'school'
when grouping ("day",origin_type_state,itcast_school_id,sc_name,payment_state)=0
then 'o_school'
when grouping ("day",origin_type_state,itcast_subject_id,name,payment_state)=0
then 'o_subject'
when grouping ("day",origin_type_state,itcast_school_id,sc_name,itcast_subject_id,name,payment_state)=0
then 'o_school_subject'
when grouping ("day",origin_type_state,origin_channel,payment_state)=0
then 'o_channel'
when grouping  ("day",origin_type_state,tdepart_id,depart_name,payment_state)=0
then 'o_zixun'
when grouping ("day",origin_type_state)=0
then 'yixiang'
when grouping ("day",origin_type_state,appeal_status)=0
then 'youxiao'
else 'other' end as group_type,
--学校学科线上线下报名人数
case when grouping ("day",origin_type_state,itcast_school_id,sc_name,itcast_subject_id,name,payment_state)=0--校区学科
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state is not null and itcast_school_id>0   and itcast_subject_id >0 and payment_state=1 ,1 ,0))
else null end as cnt_o_sc_su,
--学校线上线下报名人数
case when grouping ("day",origin_type_state,itcast_school_id,sc_name,payment_state)=0--学校
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state is not null and payment_state=1 and itcast_school_id>0,1 ,0))
else null end as cnt_o_sc,
--学校报名人数
case when grouping ("day",itcast_school_id,sc_name,payment_state)=0--学校
then sum(if(id_rn=1 and id_rn0=1  and itcast_school_id>0 and payment_state=1,1 ,0))
else null end as cnt_sc,
--学科报名人数
case  when grouping ("day",origin_type_state,itcast_subject_id,name,payment_state)=0--学科
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state is not null  and itcast_subject_id >0 and payment_state=1 , 1 ,0))
else null end as cnt_o_su,
--来源渠道报名人数
case when grouping ("day",origin_type_state,origin_channel,payment_state)=0--来源渠道
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state is not null  and origin_channel is not null and payment_state=1 ,1 ,0))
else null end as cnt_qu,
--咨询中心报名人数
case when grouping ("day" ,origin_type_state,tdepart_id,depart_name,payment_state)=0--咨询中心
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state is not null  and tdepart_id is not null   and payment_state=1, 1 ,0 ))
else null end as cnt_zi,
--意向人数
case when grouping ("day",origin_type_state) =0--意向人数
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state is not null   ,1 ,0 ))
else null end as cnt_yi,
--有效人数
case when grouping ("day",origin_type_state,appeal_status)=0--有效人数
then sum(if(id_rn=1 and id_rn0=1 and origin_type_state=1 and appeal_status<>1 and origin_type_state=1 ,1 ,0))
else null end as cnt_you

from a
group by
grouping sets (
  ("day",itcast_school_id,sc_name,payment_state),
  ("day",origin_type_state,itcast_school_id,sc_name,payment_state),
  ("day",origin_type_state,itcast_subject_id,name,payment_state),
  ("day",origin_type_state,itcast_school_id,sc_name,itcast_subject_id,name,payment_state),
  ("day",origin_type_state,origin_channel,payment_state),
  ("day",origin_type_state,tdepart_id,depart_name,payment_state),
  ("day",origin_type_state),
  ("day",origin_type_state,appeal_status)
    );


