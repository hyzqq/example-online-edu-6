insert into edu_dws.student_class_situation
with t as (select *,
                  case
                      when cast(date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)
                          between cast(morning_begin_time as time) - interval '40' minute and cast(morning_begin_time as time) + interval '10' minute
                          then 1
                      else null end as moring_effective_attendance,--上午有效出勤时间段
                  case
                      when cast(date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)
                          between cast(afternoon_begin_time as time) - interval '40' minute and cast(afternoon_begin_time as time) + interval '10' minute
                          then 2
                      else null end as afternoon_effective_attendance,--中午有效出勤时间段
                  case
                      when cast(date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)
                          between cast(evening_begin_time as time) - interval '40' minute and cast(evening_begin_time as time) + interval '10' minute
                          then 3
                      else null end as evening_effective_attendance
           from hive.edu_dwb.tbh_student_signin_record_detail),--晚上有效出勤时间段
     t1 as (select *,
                   row_number() over(partition by class_id,student_id,class_date,moring_effective_attendance) rn1,
                    row_number() over(partition by class_id,student_id,class_date,afternoon_effective_attendance) rn2,
                    row_number() over(partition by class_id,student_id,class_date,evening_effective_attendance) rn3
            from t),
     --上午出勤人数
moring_attendance_cnt as (select class_id,
                                      studying_student_count,
                                      class_date,
                                      moring_effective_attendance,
                                      count(student_id) cnt
                               from t1
                               where rn1 = 1
                                 and moring_effective_attendance = 1
                                 and share_state = 1
                                 and content is not null
                                 and content !='开班典礼'
group by class_id, moring_effective_attendance, studying_student_count, class_date) ,
    afternoon_attendance_cnt as (
select class_id,
    studying_student_count,
    class_date,
    afternoon_effective_attendance,
    count (student_id) cnt
from t1
where rn2 = 1
  and afternoon_effective_attendance = 2
  and share_state = 1
  and content is not null
  and content !='开班典礼'
group by class_id, afternoon_effective_attendance, class_date, studying_student_count),
    evening_attendance_cnt as (
select class_id,
    studying_student_count,
    class_date,
    evening_effective_attendance,
    count (student_id) cnt
from t1
where rn3 = 1
  and evening_effective_attendance = 3
  and share_state = 1
  and content is not null
  and content !='开班典礼'
group by class_id, evening_effective_attendance, class_date, studying_student_count),
    --迟到出勤
    t2 as (
select *,
    case
    when cast (date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)
    between cast (morning_begin_time as time) - interval '40' minute and cast (morning_end_time as time)
    then 1
    else null end as moring_arrive_late_attendance,--上午迟到出勤时间段
    case
    when cast (date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)
    between cast (afternoon_begin_time as time) - interval '40' minute and cast (afternoon_end_time as time)
    then 2
    else null end as afternoon_arrive_late_attendance,--中午迟到出勤时间段
    case
    when cast (date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)
    between cast (evening_begin_time as time) - interval '40' minute and cast (evening_end_time as time)
    then 3
    else null end as evening_arrive_late_attendance
from hive.edu_dwb.tbh_student_signin_record_detail),--下午迟到出勤时间段
    t3 as (
select *,
    row_number() over(partition by t2.class_id, t2.student_id, t2.class_date, moring_arrive_late_attendance order by signin_time) rn1,
    row_number() over(partition by t2.class_id, t2.student_id, t2.class_date, afternoon_arrive_late_attendance order by signin_time) rn2,
    row_number() over(partition by t2.class_id, t2.student_id, t2.class_date, evening_arrive_late_attendance order by signin_time) rn3
from t2  ) ,
  --上午迟到出勤
moring_arrive_late_cnt as (
select class_id,
    class_date,
    studying_student_count,
    moring_arrive_late_attendance,
    count (student_id) cnt
from t3
where rn1 = 1  and cast(date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)> (cast(morning_begin_time as time) + interval '10' minute)
  and moring_arrive_late_attendance = 1
  and share_state = 1
  and content is not null
  and content !='开班典礼'
group by class_id, class_date, moring_arrive_late_attendance, studying_student_count
) ,
    afternoon_arrive_late_cnt as (
select
    class_id,
    studying_student_count,
    class_date,
    afternoon_arrive_late_attendance,
    count (student_id) cnt
from t3
where rn2 = 1  and cast(date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)> (cast(afternoon_begin_time as time) + interval '10' minute)
  and afternoon_arrive_late_attendance = 2
  and share_state = 1
  and content is not null
  and content !='开班典礼'
group by class_id, class_date, afternoon_arrive_late_attendance,studying_student_count
    ) ,
    evening_arrive_late_cnt as (
select class_id,
    studying_student_count,
    class_date,
    evening_arrive_late_attendance,
    count (student_id) cnt
from t3
where rn2 = 1  and cast(date_format(parse_datetime(signin_time, 'yyyy-MM-dd HH:mm:ss'), '%H:%i:%s') as time)> (cast(evening_begin_time as time) + interval '10' minute)
  and evening_arrive_late_attendance = 3
  and share_state = 1
  and content is not null
  and content !='开班典礼'
group by class_id, class_date, evening_arrive_late_attendance, studying_student_count),
--请假人数

-- 选择班级ID和学生请假开始日期的日期部分作为请假日期
-- SELECT
--     class_id,
--     DATE(begin_time) AS leave_date,
--     -- 计算上午请假人数，考虑跨天的情况
--     SUM(
--         CASE
--             -- 如果请假开始时间在上午上课之前，并且请假结束时间在上午下课之后，则算作上午请假
--             WHEN TIME(begin_time) <= cast(morning_end_time as time) AND TIME(end_time) >= cast(morning_begin_time as time) THEN 1
--             ELSE 0
--         END
--     ) AS morning_leave_count
-- FROM
--     act_student_leave_apply_detail
-- WHERE
--     -- 确保请假开始日期和结束日期在同一天，或者请假结束时间跨过上午上课之前
--     (DATE(begin_time) = DATE(end_time))
--     OR
--     (DATE(begin_time) <> DATE(end_time)
--      AND TIME(begin_time) >= cast(morning_end_time as time))
-- GROUP BY
--     -- 按班级和请假日期分组
--     class_id,
--     leave_date;

moring_take_off_cnt as (
select class_id,
    studying_student_count,
    date (begin_time) as class_date,
    count (leave_student_id) cnt
from hive.edu_dwb.act_student_leave_apply_detail
where begin_time_type ='上午'
group by class_id, date (begin_time), studying_student_count
    ),
    afternoon_take_off_cnt as (
select class_id,
    studying_student_count,
    date (begin_time) as class_date,
    count (leave_student_id) cnt
from hive.edu_dwb.act_student_leave_apply_detail
where begin_time_type ='下午' or end_time_type='下午'
group by class_id, date (begin_time), studying_student_count),
    evening_take_off_cnt as (
select class_id,
    date (begin_time) as class_date,
    studying_student_count,
    count (leave_student_id) cnt
from hive.edu_dwb.act_student_leave_apply_detail
where begin_time_type !='上午' and begin_time_type !='下午' or (end_time_type!='上午' and end_time_type!='下午' )
group by class_id, studying_student_count, date (begin_time)
    ),
-- 旷课人数
--上午旷课
    t4 as (
select a.class_id, a.studying_student_count o, b.class_date, coalesce (b.cnt, 0) x, coalesce (e.cnt, 0) y, coalesce (d.cnt, 0) z
from hive.edu_dwd.dim_class_studying_student_count a
    left join moring_attendance_cnt b on a.class_id=b.class_id and a.studying_date=b.class_date
    left join moring_arrive_late_cnt e on a.class_id=e.class_id and a.studying_date=e.class_date
    left join moring_take_off_cnt d on a.class_id=d.class_id) ,
--下午旷课
    t5 as (
select a.class_id, a.studying_student_count o, b.class_date, coalesce (b.cnt, 0) x, coalesce (e.cnt, 0) y, coalesce (d.cnt, 0) z
from hive.edu_dwd.dim_class_studying_student_count a
    left join afternoon_attendance_cnt b
on a.class_id=b.class_id and a.studying_date=b.class_date
    left join afternoon_arrive_late_cnt e on a.class_id=e.class_id and a.studying_date=e.class_date
    left join afternoon_take_off_cnt d on a.class_id=d.class_id),
--晚上旷课
    t6 as (
select a.class_id, a.studying_student_count o, b.class_date, coalesce (b.cnt, 0) x, coalesce (e.cnt, 0) y, coalesce (d.cnt, 0) z
from hive.edu_dwd.dim_class_studying_student_count a
    left join evening_attendance_cnt b
on a.class_id=b.class_id and a.studying_date=b.class_date
    left join evening_arrive_late_cnt e on a.class_id=e.class_id and a.studying_date=e.class_date
    left join evening_take_off_cnt d on a.class_id=d.class_id),

    --上午旷课人数
    moring_volume_up_truancy_cnt as (
select t4.class_id, t4.class_date, o, o-x-y-z as moring_volume_up_truancy_cnt
from t4
where class_date is not null),
  --下午旷课人数
  afternoon_volume_up_truancy_cnt as (
select t5.class_id, t5.class_date, o, o-x-y-z as afternoon_volume_up_truancy_cnt
from t5
where class_date is not null),
  --晚上旷课人数
  evening_volume_up_truancy_cnt as (
select t6.class_id, t6.class_date, o, o-x-y-z as evening_volume_up_truancy_cnt
from t6
where class_date is not null),
--出勤率
    moring_attendance_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from moring_attendance_cnt ),
    afternoon_attendance_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from afternoon_attendance_cnt),
    evening_attendance_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from evening_attendance_cnt),
--迟到率
    moring_arrive_late_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from moring_arrive_late_cnt),
    afternoon_arrive_late_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from afternoon_arrive_late_cnt),
    evening_arrive_late_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from evening_arrive_late_cnt),
--请假率
    moring_take_off_cnt_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from moring_take_off_cnt),
    afternoon_take_off_cnt_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from afternoon_take_off_cnt),
    evening_take_off_cnt_pre as (
select class_id, class_date, round(CAST ( cnt AS DOUBLE) / CAST (studying_student_count AS DOUBLE)*100, 2) as pre
from evening_take_off_cnt),
--旷课率
    moring_volume_up_truancy_pre as (
select class_id, class_date, round(CAST ( moring_volume_up_truancy_cnt AS DOUBLE) / CAST (o AS DOUBLE)*100, 2) as pre
from moring_volume_up_truancy_cnt),
    afternoon_volume_up_truancy_pre as (
select class_id, class_date, round(CAST ( afternoon_volume_up_truancy_cnt AS DOUBLE) / CAST (o AS DOUBLE)*100, 2) as pre
from afternoon_volume_up_truancy_cnt),
    evening_volume_up_truancy_pre as (
select class_id, class_date, round(CAST ( evening_volume_up_truancy_cnt AS DOUBLE) / CAST (o AS DOUBLE)*100, 2) as pre
from evening_volume_up_truancy_cnt)

select a.class_id,
       a.studying_student_count,
       a.studying_date as class_date,
       cast(b.cnt as int) as moring_attendance_cnt,
       cast(c.pre as int) as moring_attendance_pre,
       cast(d.cnt as int) as afternoon_attendance_cnt,
       cast(e.pre as int) as afternoon_attendance_pre,
       cast(f.cnt as int) as evening_attendance_cnt,
       cast(g.pre as int) as evening_attendance_pre,
       cast(h.cnt as int) as moring_arrive_late_cnt,
       cast(i.pre as int) as moring_arrive_late_pre,
       cast(j.cnt as int) as afternoon_arrive_late_cnt,
       cast(k.pre as int) as afternoon_arrive_late_pre,
       cast(l.cnt as int) as evening_arrive_late_cnt,
       cast(m.pre as int) as evening_arrive_late_pre,
       cast(n.cnt as int) as moring_take_off_cnt,
       cast(o.pre as int) as moring_take_off_pre,
       cast(p.cnt as int) as afternoon_take_off_cnt,
       cast(q.pre as int) as afternoon_take_off_pre,
       cast(r.cnt as int) as evening_take_off_cnt,
       cast(s.pre as int) as evening_take_off_pre,
       cast (t.moring_volume_up_truancy_cnt as int),
       cast(u.pre as int) as moring_volume_up_truancy_pre,
       cast(v.afternoon_volume_up_truancy_cnt as int),
       cast(w.pre as int) as afternoon_volume_up_truancy_pre,
       cast(x.evening_volume_up_truancy_cnt as int),
       cast(y.pre as int) as evening_volume_up_truancy_pre
FROM hive.edu_dwd.dim_class_studying_student_count a
         LEFT JOIN moring_attendance_cnt b ON a.class_id = b.class_id AND a.studying_date = b.class_date
         LEFT JOIN moring_attendance_pre c ON a.class_id = c.class_id AND a.studying_date = c.class_date
         LEFT JOIN afternoon_attendance_cnt d ON a.class_id = d.class_id AND a.studying_date = d.class_date
         LEFT JOIN afternoon_attendance_pre e ON a.class_id = e.class_id AND a.studying_date = e.class_date
         LEFT JOIN evening_attendance_cnt f ON a.class_id = f.class_id AND a.studying_date = f.class_date
         LEFT JOIN evening_attendance_pre g ON a.class_id = g.class_id AND a.studying_date = g.class_date
         LEFT JOIN moring_arrive_late_cnt h ON a.class_id = h.class_id AND a.studying_date = h.class_date
         LEFT JOIN moring_arrive_late_pre i ON a.class_id = i.class_id AND a.studying_date = i.class_date
         LEFT JOIN afternoon_arrive_late_cnt j ON a.class_id = j.class_id AND a.studying_date = j.class_date
         LEFT JOIN afternoon_arrive_late_pre k ON a.class_id = k.class_id AND a.studying_date = k.class_date
         LEFT JOIN evening_arrive_late_cnt l ON a.class_id = l.class_id AND a.studying_date = l.class_date
         LEFT JOIN evening_arrive_late_pre m ON a.class_id = m.class_id AND a.studying_date = m.class_date
         LEFT JOIN moring_take_off_cnt n ON a.class_id = n.class_id AND a.studying_date = n.class_date
         LEFT JOIN moring_take_off_cnt_pre o ON a.class_id = o.class_id AND a.studying_date = o.class_date
         LEFT JOIN afternoon_take_off_cnt p ON a.class_id = p.class_id AND a.studying_date = p.class_date
         LEFT JOIN afternoon_take_off_cnt_pre q ON a.class_id = q.class_id AND a.studying_date = q.class_date
         LEFT JOIN evening_take_off_cnt r ON a.class_id = r.class_id AND a.studying_date = r.class_date
         LEFT JOIN evening_take_off_cnt_pre s ON a.class_id = s.class_id AND a.studying_date = s.class_date
         LEFT JOIN moring_volume_up_truancy_cnt t ON a.class_id = t.class_id AND a.studying_date = t.class_date
         LEFT JOIN moring_volume_up_truancy_pre u ON a.class_id = u.class_id AND a.studying_date = u.class_date
         LEFT JOIN afternoon_volume_up_truancy_cnt v ON a.class_id = v.class_id AND a.studying_date = v.class_date
         LEFT JOIN afternoon_volume_up_truancy_pre w ON a.class_id = w.class_id AND a.studying_date = w.class_date
         LEFT JOIN evening_volume_up_truancy_cnt x ON a.class_id = x.class_id AND a.studying_date = x.class_date
         LEFT JOIN evening_volume_up_truancy_pre y ON a.class_id = y.class_id AND a.studying_date = y.class_date
WHERE a.studying_date IS NOT NULL;











