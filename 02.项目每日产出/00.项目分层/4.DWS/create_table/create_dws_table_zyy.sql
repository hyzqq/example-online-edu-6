-- todo 每天线上线下各个校区各个学科的报名人数、意向人数、有效线索人数
drop table if exists edu_dws.dws_customer_detail_cnt;
create table edu_dws.dws_customer_detail_cnt
(
appeal_status int comment '有效线索',
payment_state int comment '支付状态',
itcast_school_id int comment '校区ID',
sc_name string comment '校去名称',
itcast_subject_id int comment '学科ID',
name string comment '学科名称',
origin_type_state int comment '线上线下，0未线下，1未线上',
origin_channel string comment '来源渠道',
tdepart_id int comment '咨询中心ID',
depart_name string comment '咨询中心名称',
`day` string comment '天',
group_type string comment '分组标记',
cnt_o_sc_su bigint comment '统计线上线下校区学科报名人数',
cnt_o_sc bigint comment '统计线上线下校区报名人数',
cnt_sc bigint comment '统计校区报名人数',
cnt_o_su bigint comment '统计线上线下学科报名人数',
cnt_qu bigint comment '统计渠道报名人数',
cnt_zi bigint comment '统计咨询中心报名人数',
cnt_yi bigint comment '统计意向人数',
cnt_you bigint comment '统计有效线索人数'

)comment '每天线上线下各个校区各个学科的报名人数、意向人数、有效线索人数'
ROW format delimited fields terminated BY '\t'
stored AS orc tblproperties ('orc.compress' = 'SNAPPY');
