--考勤分析表
create table edu_dws.student_class_situation
(
    class_id                        int comment '班级id',
    studying_student_count          int comment '在读班级人数',
    class_date                      date comment '上课日期',
-- 指标
    moring_attendance_cnt           int comment '上午出勤人数',
    moring_attendance_pre           int comment '上午出勤率',
    afternoon_attendance_cnt        int comment '中午出勤人数',
    afternoon_attendance_pre        int comment '中午出勤率',
    evening_attendance_cnt          int comment '晚上出勤人数',
    evening_attendance_pre          int comment '晚上出勤率',

    moring_arrive_late_cnt          int comment '上午迟到人数',
    moring_arrive_late_pre          int comment '上午迟到率',
    afternoon_arrive_late_cnt       int comment '中午迟到人数',
    afternoon_arrive_late_pre       int comment '中午迟到率',
    evening_arrive_late_cnt         int comment '晚上迟到人数',
    evening_arrive_late_pre         int comment '晚上迟到率',

    moring_take_off_cnt             int comment '上午请假人数',
    moring_take_off_pre             int comment '上午请假率',
    afternoon_take_off_cnt          int comment '中午请假人数',
    afternoon_take_off_pre          int comment '中午请假率',
    evening_take_off_cnt            int comment '晚上请假人数',
    evening_take_off_pre            int comment '晚上请假率',

    moring_volume_up_truancy_cnt    int comment '上午旷课人数',
    moring_volume_up_truancy_pre    int comment '上午旷课率',
    afternoon_volume_up_truancy_cnt int comment '中午旷课人数',
    afternoon_volume_up_truancy_pre int comment '中午旷课率',
    evening_volume_up_truancy_cnt   int comment '晚上旷课人数',
    evening_truancy_pre             int comment '晚上旷课率'

--     arrive_late_cnt        string comment '迟到人数',
--     arrive_late_pre        string comment '迟到率',
--     take_off_cnt           string comment '请假人数',
--     take_off_pre           string comment '请假率',
--     volume_up_truancy_cnt  string comment '旷课人数',
--     volume_up_truancy_pre  string comment '旷课率'
) comment '考勤打卡信息表'

    row format delimited fields terminated by '\t'
    stored as orc
    tblproperties ('orc.compress' = 'SNAPPY');