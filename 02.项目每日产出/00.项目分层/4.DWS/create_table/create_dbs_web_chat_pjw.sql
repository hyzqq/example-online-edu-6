drop table edu_dws.dws_web_chat_ems_daycount;
create table if not exists edu_dws.dws_web_chat_ems_daycount
(
    -- 维度
    date_hour      string comment '小时维度',
    country        string comment '所在国家',
    province       string comment '省',
    city           string comment '城市',
    origin_channel string comment '来源渠道(广告)',
    seo_source     string comment '搜索来源',
    from_url       string comment '会话来源页面',
    group_type     string comment '分组标记',
    --   =======日统计=======
    user_cnt       bigint comment '总访问用户量',
    ip_cnt         bigint comment '总访问ip个数',
    session_cnt    bigint comment '总访问session个数',
    consul_cnt     bigint comment '咨询人数'
) partitioned by (dt string)
    row format delimited fields terminated by '/t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');