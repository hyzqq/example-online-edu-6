-- 总访问客户量建表语句
drop table if exists edu_olap.olap_user_cnt;
create table if not exists edu_olap.olap_user_cnt(
    date_type varchar comment '时间类型',
    time_type varchar comment '分组标记',
    user_nums bigint
);

-- 地区独立访客热力图建表语句
drop table if exists edu_olap.olap_distri_cnt;
create table if not exists edu_olap.olap_distri_cnt(
    distr_type varchar comment '地区类型',
    group_type varchar comment '分组标记',
    user_nums bigint
);

-- 访客咨询率趋势建表语句
drop table if exists edu_olap.olap_consul_rate_cnt;
create table if not exists edu_olap.olap_consul_rate_cnt(
    date_type varchar comment '时间',
    time_type varchar comment '时间粒度',
    group_type varchar comment '地区粒度',
    distr_type varchar comment '地区',
    consul_rate decimal(32,2) comment '咨询率'
);

-- 时间段访问客户量趋势建表语句

drop table if exists edu_olap.olap_hour_cnt;
create table if not exists edu_olap.olap_hour_cnt(
    date_hour varchar comment '天-小时',
    user_nums bigint
);
-- 来源渠道访问量占比建表语句

drop table if exists edu_olap.olap_origin_cnt;
create table if not exists edu_olap.olap_origin_cnt(
    date_type varchar comment '时间类型',
    origin_channel varchar comment '来源渠道',
    rate decimal(22,2) comment '比率'
);

-- 搜索来源访问量占比建表语句
drop table if exists edu_olap.olap_source_cnt;
create table if not exists edu_olap.olap_source_cnt(
    date_type varchar comment '时间类型',
    seo_source varchar comment '搜索来源',
    rate decimal(22,2) comment '比率'
);

-- 活跃页面排行榜建表语句
drop table if exists edu_olap.olap_from_url_cnt;
create table if not exists edu_olap.olap_from_url_cnt(
    time_type varchar comment '时间标记',
    date_type varchar comment '时间类型',
    from_url varchar comment '来源页面',
    nums bigint comment '用户人数',
    rn decimal(22,2) comment '排名'
);

